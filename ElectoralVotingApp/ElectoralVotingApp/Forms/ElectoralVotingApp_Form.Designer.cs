﻿namespace ElectoralVotingApp
{
    partial class ElectoralVotingApp_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ElectoralVotingApp_Form));
            this.Home_Panel = new System.Windows.Forms.Panel();
            this.Home_Exit_Button = new System.Windows.Forms.Button();
            this.Home_Credits_Button = new System.Windows.Forms.Button();
            this.Home_About_Button = new System.Windows.Forms.Button();
            this.Home_Help_Button = new System.Windows.Forms.Button();
            this.Home_Load_Button = new System.Windows.Forms.Button();
            this.Home_Start_Button = new System.Windows.Forms.Button();
            this.Home_Label = new System.Windows.Forms.Label();
            this.Program_MenuStrip = new System.Windows.Forms.MenuStrip();
            this.MenuStrip_File = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Start_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Load_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Exit_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_About = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_About_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Credits_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Walkthrough_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Documentation_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_Help_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.Setup_Panel = new System.Windows.Forms.Panel();
            this.Setup_Clear_Button = new System.Windows.Forms.Button();
            this.Setup_CandidateInformation_Scrollbox = new System.Windows.Forms.Panel();
            this.Setup_CandidateCount_Textbox = new System.Windows.Forms.TextBox();
            this.Setup_CandidateCount_Label = new System.Windows.Forms.Label();
            this.Setup_Back_Button = new System.Windows.Forms.Button();
            this.Setup_Next_Button = new System.Windows.Forms.Button();
            this.Setup_Create_Button = new System.Windows.Forms.Button();
            this.Setup_Label = new System.Windows.Forms.Label();
            this.Voting_Panel = new System.Windows.Forms.Panel();
            this.Voting_Clear_Button = new System.Windows.Forms.Button();
            this.Voting_CandidatePreference_Scrollbox = new System.Windows.Forms.Panel();
            this.Voting_Back_Button = new System.Windows.Forms.Button();
            this.Voting_Submit_Button = new System.Windows.Forms.Button();
            this.Voting_Label = new System.Windows.Forms.Label();
            this.SpreadSheet_Panel = new System.Windows.Forms.Panel();
            this.SpreadSheet_InvalidVotes_Text = new System.Windows.Forms.Label();
            this.SpreadSheet_TotalVotes_Text = new System.Windows.Forms.Label();
            this.SpreadSheet_CandidateCount_Text = new System.Windows.Forms.Label();
            this.SpreadSheet_TotalVotes_Label = new System.Windows.Forms.Label();
            this.SpreadSheet_InvalidVotes_Label = new System.Windows.Forms.Label();
            this.SpreadSheet_CandidateCount_Label = new System.Windows.Forms.Label();
            this.SpreadSheet_DataGridView = new System.Windows.Forms.DataGridView();
            this.SpreadSheet_GetStatistics_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_RemoveVote_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_EditVote_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_AddVote_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_Load_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_ChangeCandidates_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_Save_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_Finish_Button = new System.Windows.Forms.Button();
            this.SpreadSheet_Label = new System.Windows.Forms.Label();
            this.Statistics_Panel = new System.Windows.Forms.Panel();
            this.Statistics_TabPanel = new System.Windows.Forms.TabControl();
            this.Statistics_Back_Button = new System.Windows.Forms.Button();
            this.Statistics_Label = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.About_Panel = new System.Windows.Forms.Panel();
            this.About_Information_Panel = new System.Windows.Forms.Panel();
            this.About_Information_Label = new System.Windows.Forms.Label();
            this.About_Back_Button = new System.Windows.Forms.Button();
            this.About_Label = new System.Windows.Forms.Label();
            this.Credits_Panel = new System.Windows.Forms.Panel();
            this.Credits_Information_Panel = new System.Windows.Forms.Panel();
            this.Credits_Information_Label = new System.Windows.Forms.Label();
            this.Credits_Back_Button = new System.Windows.Forms.Button();
            this.Credits_Label = new System.Windows.Forms.Label();
            this.Help_Panel = new System.Windows.Forms.Panel();
            this.Help_Information_Label = new System.Windows.Forms.Label();
            this.Help_Documentation_Button = new System.Windows.Forms.Button();
            this.Help_Walkthrough_Button = new System.Windows.Forms.Button();
            this.Help_Back_Button = new System.Windows.Forms.Button();
            this.Help_Label = new System.Windows.Forms.Label();
            this.Home_Panel.SuspendLayout();
            this.Program_MenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.Setup_Panel.SuspendLayout();
            this.Voting_Panel.SuspendLayout();
            this.SpreadSheet_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpreadSheet_DataGridView)).BeginInit();
            this.Statistics_Panel.SuspendLayout();
            this.About_Panel.SuspendLayout();
            this.About_Information_Panel.SuspendLayout();
            this.Credits_Panel.SuspendLayout();
            this.Credits_Information_Panel.SuspendLayout();
            this.Help_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Home_Panel
            // 
            this.Home_Panel.Controls.Add(this.Home_Exit_Button);
            this.Home_Panel.Controls.Add(this.Home_Credits_Button);
            this.Home_Panel.Controls.Add(this.Home_About_Button);
            this.Home_Panel.Controls.Add(this.Home_Help_Button);
            this.Home_Panel.Controls.Add(this.Home_Load_Button);
            this.Home_Panel.Controls.Add(this.Home_Start_Button);
            this.Home_Panel.Controls.Add(this.Home_Label);
            this.Home_Panel.Location = new System.Drawing.Point(-2, 42);
            this.Home_Panel.Name = "Home_Panel";
            this.Home_Panel.Size = new System.Drawing.Size(780, 500);
            this.Home_Panel.TabIndex = 0;
            // 
            // Home_Exit_Button
            // 
            this.Home_Exit_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_Exit_Button.Location = new System.Drawing.Point(312, 409);
            this.Home_Exit_Button.Name = "Home_Exit_Button";
            this.Home_Exit_Button.Size = new System.Drawing.Size(150, 40);
            this.Home_Exit_Button.TabIndex = 5;
            this.Home_Exit_Button.Text = "Exit";
            this.Home_Exit_Button.UseVisualStyleBackColor = true;
            this.Home_Exit_Button.Click += new System.EventHandler(this.Home_Exit_Button_Click);
            // 
            // Home_Credits_Button
            // 
            this.Home_Credits_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_Credits_Button.Location = new System.Drawing.Point(312, 326);
            this.Home_Credits_Button.Name = "Home_Credits_Button";
            this.Home_Credits_Button.Size = new System.Drawing.Size(150, 40);
            this.Home_Credits_Button.TabIndex = 4;
            this.Home_Credits_Button.Text = "Credits";
            this.Home_Credits_Button.UseVisualStyleBackColor = true;
            this.Home_Credits_Button.Click += new System.EventHandler(this.Home_Credits_Button_Click);
            // 
            // Home_About_Button
            // 
            this.Home_About_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_About_Button.Location = new System.Drawing.Point(312, 280);
            this.Home_About_Button.Name = "Home_About_Button";
            this.Home_About_Button.Size = new System.Drawing.Size(150, 40);
            this.Home_About_Button.TabIndex = 3;
            this.Home_About_Button.Text = "About";
            this.Home_About_Button.UseVisualStyleBackColor = true;
            this.Home_About_Button.Click += new System.EventHandler(this.Home_About_Button_Click);
            // 
            // Home_Help_Button
            // 
            this.Home_Help_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_Help_Button.Location = new System.Drawing.Point(312, 234);
            this.Home_Help_Button.Name = "Home_Help_Button";
            this.Home_Help_Button.Size = new System.Drawing.Size(150, 40);
            this.Home_Help_Button.TabIndex = 2;
            this.Home_Help_Button.Text = "Help";
            this.Home_Help_Button.UseVisualStyleBackColor = true;
            this.Home_Help_Button.Click += new System.EventHandler(this.Home_Help_Button_Click);
            // 
            // Home_Load_Button
            // 
            this.Home_Load_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_Load_Button.Location = new System.Drawing.Point(312, 148);
            this.Home_Load_Button.Name = "Home_Load_Button";
            this.Home_Load_Button.Size = new System.Drawing.Size(150, 40);
            this.Home_Load_Button.TabIndex = 1;
            this.Home_Load_Button.Text = "Load";
            this.Home_Load_Button.UseVisualStyleBackColor = true;
            this.Home_Load_Button.Click += new System.EventHandler(this.Home_Load_Button_Click);
            // 
            // Home_Start_Button
            // 
            this.Home_Start_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_Start_Button.Location = new System.Drawing.Point(312, 102);
            this.Home_Start_Button.Name = "Home_Start_Button";
            this.Home_Start_Button.Size = new System.Drawing.Size(150, 40);
            this.Home_Start_Button.TabIndex = 0;
            this.Home_Start_Button.Text = "Start";
            this.Home_Start_Button.UseVisualStyleBackColor = true;
            this.Home_Start_Button.Click += new System.EventHandler(this.Home_Start_Button_Click);
            // 
            // Home_Label
            // 
            this.Home_Label.AutoSize = true;
            this.Home_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_Label.Location = new System.Drawing.Point(190, 14);
            this.Home_Label.Name = "Home_Label";
            this.Home_Label.Size = new System.Drawing.Size(409, 37);
            this.Home_Label.TabIndex = 0;
            this.Home_Label.Text = "Electoral Voting Application";
            this.Home_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Program_MenuStrip
            // 
            this.Program_MenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.Program_MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_File,
            this.MenuStrip_About,
            this.MenuStrip_Help});
            this.Program_MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.Program_MenuStrip.Name = "Program_MenuStrip";
            this.Program_MenuStrip.Size = new System.Drawing.Size(776, 33);
            this.Program_MenuStrip.TabIndex = 1;
            this.Program_MenuStrip.Text = "menuStrip1";
            // 
            // MenuStrip_File
            // 
            this.MenuStrip_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_Start_Button,
            this.MenuStrip_Load_Button,
            this.importToolStripMenuItem,
            this.MenuStrip_Exit_Button});
            this.MenuStrip_File.Name = "MenuStrip_File";
            this.MenuStrip_File.Size = new System.Drawing.Size(50, 29);
            this.MenuStrip_File.Text = "File";
            // 
            // MenuStrip_Start_Button
            // 
            this.MenuStrip_Start_Button.Name = "MenuStrip_Start_Button";
            this.MenuStrip_Start_Button.Size = new System.Drawing.Size(152, 30);
            this.MenuStrip_Start_Button.Text = "Start";
            this.MenuStrip_Start_Button.Click += new System.EventHandler(this.Home_Start_Button_Click);
            // 
            // MenuStrip_Load_Button
            // 
            this.MenuStrip_Load_Button.Name = "MenuStrip_Load_Button";
            this.MenuStrip_Load_Button.Size = new System.Drawing.Size(152, 30);
            this.MenuStrip_Load_Button.Text = "Load";
            this.MenuStrip_Load_Button.Click += new System.EventHandler(this.Home_Load_Button_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(152, 30);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.Home_Import_Button_Click);
            // 
            // MenuStrip_Exit_Button
            // 
            this.MenuStrip_Exit_Button.Name = "MenuStrip_Exit_Button";
            this.MenuStrip_Exit_Button.Size = new System.Drawing.Size(152, 30);
            this.MenuStrip_Exit_Button.Text = "Exit";
            this.MenuStrip_Exit_Button.Click += new System.EventHandler(this.Home_Exit_Button_Click);
            // 
            // MenuStrip_About
            // 
            this.MenuStrip_About.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_About_Button,
            this.MenuStrip_Credits_Button});
            this.MenuStrip_About.Name = "MenuStrip_About";
            this.MenuStrip_About.Size = new System.Drawing.Size(74, 29);
            this.MenuStrip_About.Text = "About";
            // 
            // MenuStrip_About_Button
            // 
            this.MenuStrip_About_Button.Name = "MenuStrip_About_Button";
            this.MenuStrip_About_Button.Size = new System.Drawing.Size(152, 30);
            this.MenuStrip_About_Button.Text = "About";
            this.MenuStrip_About_Button.Click += new System.EventHandler(this.Home_About_Button_Click);
            // 
            // MenuStrip_Credits_Button
            // 
            this.MenuStrip_Credits_Button.Name = "MenuStrip_Credits_Button";
            this.MenuStrip_Credits_Button.Size = new System.Drawing.Size(152, 30);
            this.MenuStrip_Credits_Button.Text = "Credits";
            this.MenuStrip_Credits_Button.Click += new System.EventHandler(this.Home_Credits_Button_Click);
            // 
            // MenuStrip_Help
            // 
            this.MenuStrip_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_Walkthrough_Button,
            this.MenuStrip_Documentation_Button,
            this.MenuStrip_Help_Button});
            this.MenuStrip_Help.Name = "MenuStrip_Help";
            this.MenuStrip_Help.Size = new System.Drawing.Size(61, 29);
            this.MenuStrip_Help.Text = "Help";
            // 
            // MenuStrip_Walkthrough_Button
            // 
            this.MenuStrip_Walkthrough_Button.Name = "MenuStrip_Walkthrough_Button";
            this.MenuStrip_Walkthrough_Button.Size = new System.Drawing.Size(220, 30);
            this.MenuStrip_Walkthrough_Button.Text = "Walkthrough";
            this.MenuStrip_Walkthrough_Button.Click += new System.EventHandler(this.Help_Walkthrough_Button_Click);
            // 
            // MenuStrip_Documentation_Button
            // 
            this.MenuStrip_Documentation_Button.Name = "MenuStrip_Documentation_Button";
            this.MenuStrip_Documentation_Button.Size = new System.Drawing.Size(220, 30);
            this.MenuStrip_Documentation_Button.Text = "Documentation";
            this.MenuStrip_Documentation_Button.Click += new System.EventHandler(this.Help_Documentation_Button_Click);
            // 
            // MenuStrip_Help_Button
            // 
            this.MenuStrip_Help_Button.Name = "MenuStrip_Help_Button";
            this.MenuStrip_Help_Button.Size = new System.Drawing.Size(220, 30);
            this.MenuStrip_Help_Button.Text = "Help";
            this.MenuStrip_Help_Button.Click += new System.EventHandler(this.Home_Help_Button_Click);
            // 
            // Setup_Panel
            // 
            this.Setup_Panel.Controls.Add(this.Setup_Clear_Button);
            this.Setup_Panel.Controls.Add(this.Setup_CandidateInformation_Scrollbox);
            this.Setup_Panel.Controls.Add(this.Setup_CandidateCount_Textbox);
            this.Setup_Panel.Controls.Add(this.Setup_CandidateCount_Label);
            this.Setup_Panel.Controls.Add(this.Setup_Back_Button);
            this.Setup_Panel.Controls.Add(this.Setup_Next_Button);
            this.Setup_Panel.Controls.Add(this.Setup_Create_Button);
            this.Setup_Panel.Controls.Add(this.Setup_Label);
            this.Setup_Panel.Location = new System.Drawing.Point(-2, 42);
            this.Setup_Panel.Name = "Setup_Panel";
            this.Setup_Panel.Size = new System.Drawing.Size(780, 500);
            this.Setup_Panel.TabIndex = 2;
            this.Setup_Panel.TabStop = true;
            // 
            // Setup_Clear_Button
            // 
            this.Setup_Clear_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Setup_Clear_Button.Location = new System.Drawing.Point(334, 449);
            this.Setup_Clear_Button.Name = "Setup_Clear_Button";
            this.Setup_Clear_Button.Size = new System.Drawing.Size(130, 40);
            this.Setup_Clear_Button.TabIndex = 3;
            this.Setup_Clear_Button.Text = "Clear";
            this.Setup_Clear_Button.UseVisualStyleBackColor = true;
            this.Setup_Clear_Button.Click += new System.EventHandler(this.Setup_Clear_Button_Click);
            // 
            // Setup_CandidateInformation_Scrollbox
            // 
            this.Setup_CandidateInformation_Scrollbox.AutoScroll = true;
            this.Setup_CandidateInformation_Scrollbox.Location = new System.Drawing.Point(198, 128);
            this.Setup_CandidateInformation_Scrollbox.Name = "Setup_CandidateInformation_Scrollbox";
            this.Setup_CandidateInformation_Scrollbox.Size = new System.Drawing.Size(402, 306);
            this.Setup_CandidateInformation_Scrollbox.TabIndex = 1;
            // 
            // Setup_CandidateCount_Textbox
            // 
            this.Setup_CandidateCount_Textbox.Location = new System.Drawing.Point(382, 83);
            this.Setup_CandidateCount_Textbox.Name = "Setup_CandidateCount_Textbox";
            this.Setup_CandidateCount_Textbox.Size = new System.Drawing.Size(44, 26);
            this.Setup_CandidateCount_Textbox.TabIndex = 0;
            // 
            // Setup_CandidateCount_Label
            // 
            this.Setup_CandidateCount_Label.AutoSize = true;
            this.Setup_CandidateCount_Label.Location = new System.Drawing.Point(248, 88);
            this.Setup_CandidateCount_Label.Name = "Setup_CandidateCount_Label";
            this.Setup_CandidateCount_Label.Size = new System.Drawing.Size(129, 20);
            this.Setup_CandidateCount_Label.TabIndex = 0;
            this.Setup_CandidateCount_Label.Text = "Candidate Count";
            // 
            // Setup_Back_Button
            // 
            this.Setup_Back_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Setup_Back_Button.Location = new System.Drawing.Point(198, 449);
            this.Setup_Back_Button.Name = "Setup_Back_Button";
            this.Setup_Back_Button.Size = new System.Drawing.Size(130, 40);
            this.Setup_Back_Button.TabIndex = 2;
            this.Setup_Back_Button.Text = "Back";
            this.Setup_Back_Button.UseVisualStyleBackColor = true;
            this.Setup_Back_Button.Click += new System.EventHandler(this.Setup_Back_Button_Click);
            // 
            // Setup_Next_Button
            // 
            this.Setup_Next_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Setup_Next_Button.Location = new System.Drawing.Point(468, 449);
            this.Setup_Next_Button.Name = "Setup_Next_Button";
            this.Setup_Next_Button.Size = new System.Drawing.Size(130, 40);
            this.Setup_Next_Button.TabIndex = 4;
            this.Setup_Next_Button.Text = "Next";
            this.Setup_Next_Button.UseVisualStyleBackColor = true;
            this.Setup_Next_Button.Click += new System.EventHandler(this.Setup_Next_Button_Click);
            // 
            // Setup_Create_Button
            // 
            this.Setup_Create_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Setup_Create_Button.Location = new System.Drawing.Point(450, 77);
            this.Setup_Create_Button.Name = "Setup_Create_Button";
            this.Setup_Create_Button.Size = new System.Drawing.Size(130, 40);
            this.Setup_Create_Button.TabIndex = 0;
            this.Setup_Create_Button.Text = "Create";
            this.Setup_Create_Button.UseVisualStyleBackColor = true;
            this.Setup_Create_Button.Click += new System.EventHandler(this.Setup_Create_Button_Click);
            // 
            // Setup_Label
            // 
            this.Setup_Label.AutoSize = true;
            this.Setup_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Setup_Label.Location = new System.Drawing.Point(336, 14);
            this.Setup_Label.Name = "Setup_Label";
            this.Setup_Label.Size = new System.Drawing.Size(100, 37);
            this.Setup_Label.TabIndex = 0;
            this.Setup_Label.Text = "Setup";
            this.Setup_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Voting_Panel
            // 
            this.Voting_Panel.Controls.Add(this.Voting_Clear_Button);
            this.Voting_Panel.Controls.Add(this.Voting_CandidatePreference_Scrollbox);
            this.Voting_Panel.Controls.Add(this.Voting_Back_Button);
            this.Voting_Panel.Controls.Add(this.Voting_Submit_Button);
            this.Voting_Panel.Controls.Add(this.Voting_Label);
            this.Voting_Panel.Location = new System.Drawing.Point(-2, 42);
            this.Voting_Panel.Name = "Voting_Panel";
            this.Voting_Panel.Size = new System.Drawing.Size(780, 500);
            this.Voting_Panel.TabIndex = 4;
            // 
            // Voting_Clear_Button
            // 
            this.Voting_Clear_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Voting_Clear_Button.Location = new System.Drawing.Point(198, 68);
            this.Voting_Clear_Button.Name = "Voting_Clear_Button";
            this.Voting_Clear_Button.Size = new System.Drawing.Size(150, 40);
            this.Voting_Clear_Button.TabIndex = 0;
            this.Voting_Clear_Button.Text = "Clear";
            this.Voting_Clear_Button.UseVisualStyleBackColor = true;
            this.Voting_Clear_Button.Click += new System.EventHandler(this.Voting_Clear_Button_Click);
            // 
            // Voting_CandidatePreference_Scrollbox
            // 
            this.Voting_CandidatePreference_Scrollbox.AutoScroll = true;
            this.Voting_CandidatePreference_Scrollbox.Location = new System.Drawing.Point(198, 117);
            this.Voting_CandidatePreference_Scrollbox.Name = "Voting_CandidatePreference_Scrollbox";
            this.Voting_CandidatePreference_Scrollbox.Size = new System.Drawing.Size(402, 317);
            this.Voting_CandidatePreference_Scrollbox.TabIndex = 2;
            // 
            // Voting_Back_Button
            // 
            this.Voting_Back_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Voting_Back_Button.Location = new System.Drawing.Point(198, 449);
            this.Voting_Back_Button.Name = "Voting_Back_Button";
            this.Voting_Back_Button.Size = new System.Drawing.Size(150, 40);
            this.Voting_Back_Button.TabIndex = 3;
            this.Voting_Back_Button.Text = "Back";
            this.Voting_Back_Button.UseVisualStyleBackColor = true;
            this.Voting_Back_Button.Click += new System.EventHandler(this.Voting_Back_Button_Click);
            // 
            // Voting_Submit_Button
            // 
            this.Voting_Submit_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Voting_Submit_Button.Location = new System.Drawing.Point(450, 449);
            this.Voting_Submit_Button.Name = "Voting_Submit_Button";
            this.Voting_Submit_Button.Size = new System.Drawing.Size(150, 40);
            this.Voting_Submit_Button.TabIndex = 4;
            this.Voting_Submit_Button.Text = "Submit";
            this.Voting_Submit_Button.UseVisualStyleBackColor = true;
            this.Voting_Submit_Button.Click += new System.EventHandler(this.Voting_Submit_Button_Click);
            // 
            // Voting_Label
            // 
            this.Voting_Label.AutoSize = true;
            this.Voting_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Voting_Label.Location = new System.Drawing.Point(336, 14);
            this.Voting_Label.Name = "Voting_Label";
            this.Voting_Label.Size = new System.Drawing.Size(109, 37);
            this.Voting_Label.TabIndex = 0;
            this.Voting_Label.Text = "Voting";
            this.Voting_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SpreadSheet_Panel
            // 
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_InvalidVotes_Text);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_TotalVotes_Text);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_CandidateCount_Text);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_TotalVotes_Label);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_InvalidVotes_Label);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_CandidateCount_Label);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_DataGridView);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_GetStatistics_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_RemoveVote_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_EditVote_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_AddVote_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_Load_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_ChangeCandidates_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_Save_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_Finish_Button);
            this.SpreadSheet_Panel.Controls.Add(this.SpreadSheet_Label);
            this.SpreadSheet_Panel.Location = new System.Drawing.Point(-2, 42);
            this.SpreadSheet_Panel.Name = "SpreadSheet_Panel";
            this.SpreadSheet_Panel.Size = new System.Drawing.Size(780, 500);
            this.SpreadSheet_Panel.TabIndex = 3;
            // 
            // SpreadSheet_InvalidVotes_Text
            // 
            this.SpreadSheet_InvalidVotes_Text.AutoSize = true;
            this.SpreadSheet_InvalidVotes_Text.Location = new System.Drawing.Point(606, 100);
            this.SpreadSheet_InvalidVotes_Text.Name = "SpreadSheet_InvalidVotes_Text";
            this.SpreadSheet_InvalidVotes_Text.Size = new System.Drawing.Size(18, 20);
            this.SpreadSheet_InvalidVotes_Text.TabIndex = 22;
            this.SpreadSheet_InvalidVotes_Text.Text = "0";
            // 
            // SpreadSheet_TotalVotes_Text
            // 
            this.SpreadSheet_TotalVotes_Text.AutoSize = true;
            this.SpreadSheet_TotalVotes_Text.Location = new System.Drawing.Point(606, 68);
            this.SpreadSheet_TotalVotes_Text.Name = "SpreadSheet_TotalVotes_Text";
            this.SpreadSheet_TotalVotes_Text.Size = new System.Drawing.Size(18, 20);
            this.SpreadSheet_TotalVotes_Text.TabIndex = 21;
            this.SpreadSheet_TotalVotes_Text.Text = "0";
            // 
            // SpreadSheet_CandidateCount_Text
            // 
            this.SpreadSheet_CandidateCount_Text.AutoSize = true;
            this.SpreadSheet_CandidateCount_Text.Location = new System.Drawing.Point(339, 100);
            this.SpreadSheet_CandidateCount_Text.Name = "SpreadSheet_CandidateCount_Text";
            this.SpreadSheet_CandidateCount_Text.Size = new System.Drawing.Size(18, 20);
            this.SpreadSheet_CandidateCount_Text.TabIndex = 20;
            this.SpreadSheet_CandidateCount_Text.Text = "0";
            // 
            // SpreadSheet_TotalVotes_Label
            // 
            this.SpreadSheet_TotalVotes_Label.AutoSize = true;
            this.SpreadSheet_TotalVotes_Label.Location = new System.Drawing.Point(506, 68);
            this.SpreadSheet_TotalVotes_Label.Name = "SpreadSheet_TotalVotes_Label";
            this.SpreadSheet_TotalVotes_Label.Size = new System.Drawing.Size(94, 20);
            this.SpreadSheet_TotalVotes_Label.TabIndex = 0;
            this.SpreadSheet_TotalVotes_Label.Text = "Total Votes:";
            // 
            // SpreadSheet_InvalidVotes_Label
            // 
            this.SpreadSheet_InvalidVotes_Label.AutoSize = true;
            this.SpreadSheet_InvalidVotes_Label.Location = new System.Drawing.Point(496, 100);
            this.SpreadSheet_InvalidVotes_Label.Name = "SpreadSheet_InvalidVotes_Label";
            this.SpreadSheet_InvalidVotes_Label.Size = new System.Drawing.Size(104, 20);
            this.SpreadSheet_InvalidVotes_Label.TabIndex = 18;
            this.SpreadSheet_InvalidVotes_Label.Text = "Invalid Votes:";
            // 
            // SpreadSheet_CandidateCount_Label
            // 
            this.SpreadSheet_CandidateCount_Label.AutoSize = true;
            this.SpreadSheet_CandidateCount_Label.Location = new System.Drawing.Point(170, 100);
            this.SpreadSheet_CandidateCount_Label.Name = "SpreadSheet_CandidateCount_Label";
            this.SpreadSheet_CandidateCount_Label.Size = new System.Drawing.Size(172, 20);
            this.SpreadSheet_CandidateCount_Label.TabIndex = 17;
            this.SpreadSheet_CandidateCount_Label.Text = "Number of Candidates:";
            // 
            // SpreadSheet_DataGridView
            // 
            this.SpreadSheet_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SpreadSheet_DataGridView.Location = new System.Drawing.Point(14, 131);
            this.SpreadSheet_DataGridView.Name = "SpreadSheet_DataGridView";
            this.SpreadSheet_DataGridView.RowTemplate.Height = 28;
            this.SpreadSheet_DataGridView.Size = new System.Drawing.Size(754, 235);
            this.SpreadSheet_DataGridView.TabIndex = 1;
            this.SpreadSheet_DataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_RowEnter);
            // 
            // SpreadSheet_GetStatistics_Button
            // 
            this.SpreadSheet_GetStatistics_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_GetStatistics_Button.Location = new System.Drawing.Point(618, 382);
            this.SpreadSheet_GetStatistics_Button.Name = "SpreadSheet_GetStatistics_Button";
            this.SpreadSheet_GetStatistics_Button.Size = new System.Drawing.Size(150, 40);
            this.SpreadSheet_GetStatistics_Button.TabIndex = 5;
            this.SpreadSheet_GetStatistics_Button.Text = "Get Statistics";
            this.SpreadSheet_GetStatistics_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_GetStatistics_Button.Click += new System.EventHandler(this.SpreadSheet_GetStatistics_Button_Click);
            // 
            // SpreadSheet_RemoveVote_Button
            // 
            this.SpreadSheet_RemoveVote_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_RemoveVote_Button.Location = new System.Drawing.Point(350, 382);
            this.SpreadSheet_RemoveVote_Button.Name = "SpreadSheet_RemoveVote_Button";
            this.SpreadSheet_RemoveVote_Button.Size = new System.Drawing.Size(150, 40);
            this.SpreadSheet_RemoveVote_Button.TabIndex = 4;
            this.SpreadSheet_RemoveVote_Button.Text = "Remove Vote";
            this.SpreadSheet_RemoveVote_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_RemoveVote_Button.Click += new System.EventHandler(this.SpreadSheet_RemoveVote_Button_Click);
            // 
            // SpreadSheet_EditVote_Button
            // 
            this.SpreadSheet_EditVote_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_EditVote_Button.Location = new System.Drawing.Point(182, 382);
            this.SpreadSheet_EditVote_Button.Name = "SpreadSheet_EditVote_Button";
            this.SpreadSheet_EditVote_Button.Size = new System.Drawing.Size(150, 40);
            this.SpreadSheet_EditVote_Button.TabIndex = 3;
            this.SpreadSheet_EditVote_Button.Text = "Edit Vote";
            this.SpreadSheet_EditVote_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_EditVote_Button.Click += new System.EventHandler(this.SpreadSheet_EditVote_Button_Click);
            // 
            // SpreadSheet_AddVote_Button
            // 
            this.SpreadSheet_AddVote_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_AddVote_Button.Location = new System.Drawing.Point(14, 382);
            this.SpreadSheet_AddVote_Button.Name = "SpreadSheet_AddVote_Button";
            this.SpreadSheet_AddVote_Button.Size = new System.Drawing.Size(150, 40);
            this.SpreadSheet_AddVote_Button.TabIndex = 2;
            this.SpreadSheet_AddVote_Button.Text = "Add Vote";
            this.SpreadSheet_AddVote_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_AddVote_Button.Click += new System.EventHandler(this.SpreadSheet_AddVote_Button_Click);
            // 
            // SpreadSheet_Load_Button
            // 
            this.SpreadSheet_Load_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_Load_Button.Location = new System.Drawing.Point(182, 449);
            this.SpreadSheet_Load_Button.Name = "SpreadSheet_Load_Button";
            this.SpreadSheet_Load_Button.Size = new System.Drawing.Size(150, 40);
            this.SpreadSheet_Load_Button.TabIndex = 7;
            this.SpreadSheet_Load_Button.Text = "Load";
            this.SpreadSheet_Load_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_Load_Button.Click += new System.EventHandler(this.SpreadSheet_Load_Button_Click);
            // 
            // SpreadSheet_ChangeCandidates_Button
            // 
            this.SpreadSheet_ChangeCandidates_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_ChangeCandidates_Button.Location = new System.Drawing.Point(14, 68);
            this.SpreadSheet_ChangeCandidates_Button.Name = "SpreadSheet_ChangeCandidates_Button";
            this.SpreadSheet_ChangeCandidates_Button.Size = new System.Drawing.Size(150, 52);
            this.SpreadSheet_ChangeCandidates_Button.TabIndex = 0;
            this.SpreadSheet_ChangeCandidates_Button.Text = "Change Candidates";
            this.SpreadSheet_ChangeCandidates_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_ChangeCandidates_Button.Click += new System.EventHandler(this.SpreadSheet_ChangeCandidates_Button_Click);
            // 
            // SpreadSheet_Save_Button
            // 
            this.SpreadSheet_Save_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_Save_Button.Location = new System.Drawing.Point(14, 449);
            this.SpreadSheet_Save_Button.Name = "SpreadSheet_Save_Button";
            this.SpreadSheet_Save_Button.Size = new System.Drawing.Size(150, 40);
            this.SpreadSheet_Save_Button.TabIndex = 6;
            this.SpreadSheet_Save_Button.Text = "Save";
            this.SpreadSheet_Save_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_Save_Button.Click += new System.EventHandler(this.SpreadSheet_Save_Button_Click);
            // 
            // SpreadSheet_Finish_Button
            // 
            this.SpreadSheet_Finish_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_Finish_Button.Location = new System.Drawing.Point(618, 449);
            this.SpreadSheet_Finish_Button.Name = "SpreadSheet_Finish_Button";
            this.SpreadSheet_Finish_Button.Size = new System.Drawing.Size(150, 40);
            this.SpreadSheet_Finish_Button.TabIndex = 8;
            this.SpreadSheet_Finish_Button.Text = "Finish";
            this.SpreadSheet_Finish_Button.UseVisualStyleBackColor = true;
            this.SpreadSheet_Finish_Button.Click += new System.EventHandler(this.SpreadSheet_Finish_Button_Click);
            // 
            // SpreadSheet_Label
            // 
            this.SpreadSheet_Label.AutoSize = true;
            this.SpreadSheet_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpreadSheet_Label.Location = new System.Drawing.Point(292, 14);
            this.SpreadSheet_Label.Name = "SpreadSheet_Label";
            this.SpreadSheet_Label.Size = new System.Drawing.Size(197, 37);
            this.SpreadSheet_Label.TabIndex = 0;
            this.SpreadSheet_Label.Text = "Spreadsheet";
            this.SpreadSheet_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Statistics_Panel
            // 
            this.Statistics_Panel.Controls.Add(this.Statistics_TabPanel);
            this.Statistics_Panel.Controls.Add(this.Statistics_Back_Button);
            this.Statistics_Panel.Controls.Add(this.Statistics_Label);
            this.Statistics_Panel.Location = new System.Drawing.Point(-2, 42);
            this.Statistics_Panel.Name = "Statistics_Panel";
            this.Statistics_Panel.Size = new System.Drawing.Size(780, 500);
            this.Statistics_Panel.TabIndex = 5;
            // 
            // Statistics_TabPanel
            // 
            this.Statistics_TabPanel.Location = new System.Drawing.Point(14, 54);
            this.Statistics_TabPanel.Name = "Statistics_TabPanel";
            this.Statistics_TabPanel.SelectedIndex = 0;
            this.Statistics_TabPanel.Size = new System.Drawing.Size(754, 377);
            this.Statistics_TabPanel.TabIndex = 0;
            // 
            // Statistics_Back_Button
            // 
            this.Statistics_Back_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Statistics_Back_Button.Location = new System.Drawing.Point(614, 449);
            this.Statistics_Back_Button.Name = "Statistics_Back_Button";
            this.Statistics_Back_Button.Size = new System.Drawing.Size(150, 40);
            this.Statistics_Back_Button.TabIndex = 0;
            this.Statistics_Back_Button.Text = "Back";
            this.Statistics_Back_Button.UseVisualStyleBackColor = true;
            this.Statistics_Back_Button.Click += new System.EventHandler(this.Statistics_Back_Button_Click);
            // 
            // Statistics_Label
            // 
            this.Statistics_Label.AutoSize = true;
            this.Statistics_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Statistics_Label.Location = new System.Drawing.Point(327, 14);
            this.Statistics_Label.Name = "Statistics_Label";
            this.Statistics_Label.Size = new System.Drawing.Size(145, 37);
            this.Statistics_Label.TabIndex = 0;
            this.Statistics_Label.Text = "Statistics";
            this.Statistics_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // About_Panel
            // 
            this.About_Panel.Controls.Add(this.About_Information_Panel);
            this.About_Panel.Controls.Add(this.About_Back_Button);
            this.About_Panel.Controls.Add(this.About_Label);
            this.About_Panel.Location = new System.Drawing.Point(-2, 42);
            this.About_Panel.Name = "About_Panel";
            this.About_Panel.Size = new System.Drawing.Size(780, 500);
            this.About_Panel.TabIndex = 8;
            // 
            // About_Information_Panel
            // 
            this.About_Information_Panel.AutoScroll = true;
            this.About_Information_Panel.Controls.Add(this.About_Information_Label);
            this.About_Information_Panel.Location = new System.Drawing.Point(146, 68);
            this.About_Information_Panel.Name = "About_Information_Panel";
            this.About_Information_Panel.Size = new System.Drawing.Size(478, 366);
            this.About_Information_Panel.TabIndex = 0;
            // 
            // About_Information_Label
            // 
            this.About_Information_Label.AutoSize = true;
            this.About_Information_Label.Location = new System.Drawing.Point(10, 20);
            this.About_Information_Label.Name = "About_Information_Label";
            this.About_Information_Label.Size = new System.Drawing.Size(444, 100);
            this.About_Information_Label.TabIndex = 0;
            this.About_Information_Label.Text = resources.GetString("About_Information_Label.Text");
            // 
            // About_Back_Button
            // 
            this.About_Back_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.About_Back_Button.Location = new System.Drawing.Point(312, 449);
            this.About_Back_Button.Name = "About_Back_Button";
            this.About_Back_Button.Size = new System.Drawing.Size(150, 40);
            this.About_Back_Button.TabIndex = 0;
            this.About_Back_Button.Text = "Back";
            this.About_Back_Button.UseVisualStyleBackColor = true;
            this.About_Back_Button.Click += new System.EventHandler(this.About_Back_Button_Click);
            // 
            // About_Label
            // 
            this.About_Label.AutoSize = true;
            this.About_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.About_Label.Location = new System.Drawing.Point(336, 14);
            this.About_Label.Name = "About_Label";
            this.About_Label.Size = new System.Drawing.Size(102, 37);
            this.About_Label.TabIndex = 0;
            this.About_Label.Text = "About";
            this.About_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Credits_Panel
            // 
            this.Credits_Panel.Controls.Add(this.Credits_Information_Panel);
            this.Credits_Panel.Controls.Add(this.Credits_Back_Button);
            this.Credits_Panel.Controls.Add(this.Credits_Label);
            this.Credits_Panel.Location = new System.Drawing.Point(-2, 42);
            this.Credits_Panel.Name = "Credits_Panel";
            this.Credits_Panel.Size = new System.Drawing.Size(780, 500);
            this.Credits_Panel.TabIndex = 7;
            // 
            // Credits_Information_Panel
            // 
            this.Credits_Information_Panel.AutoScroll = true;
            this.Credits_Information_Panel.Controls.Add(this.Credits_Information_Label);
            this.Credits_Information_Panel.Location = new System.Drawing.Point(146, 68);
            this.Credits_Information_Panel.Name = "Credits_Information_Panel";
            this.Credits_Information_Panel.Size = new System.Drawing.Size(478, 366);
            this.Credits_Information_Panel.TabIndex = 0;
            // 
            // Credits_Information_Label
            // 
            this.Credits_Information_Label.AutoSize = true;
            this.Credits_Information_Label.Location = new System.Drawing.Point(102, 9);
            this.Credits_Information_Label.Name = "Credits_Information_Label";
            this.Credits_Information_Label.Size = new System.Drawing.Size(281, 400);
            this.Credits_Information_Label.TabIndex = 0;
            this.Credits_Information_Label.Text = resources.GetString("Credits_Information_Label.Text");
            this.Credits_Information_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Credits_Back_Button
            // 
            this.Credits_Back_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Credits_Back_Button.Location = new System.Drawing.Point(312, 449);
            this.Credits_Back_Button.Name = "Credits_Back_Button";
            this.Credits_Back_Button.Size = new System.Drawing.Size(150, 40);
            this.Credits_Back_Button.TabIndex = 0;
            this.Credits_Back_Button.Text = "Back";
            this.Credits_Back_Button.UseVisualStyleBackColor = true;
            this.Credits_Back_Button.Click += new System.EventHandler(this.Credits_Back_Button_Click);
            // 
            // Credits_Label
            // 
            this.Credits_Label.AutoSize = true;
            this.Credits_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Credits_Label.Location = new System.Drawing.Point(336, 14);
            this.Credits_Label.Name = "Credits_Label";
            this.Credits_Label.Size = new System.Drawing.Size(118, 37);
            this.Credits_Label.TabIndex = 0;
            this.Credits_Label.Text = "Credits";
            this.Credits_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Help_Panel
            // 
            this.Help_Panel.Controls.Add(this.Help_Information_Label);
            this.Help_Panel.Controls.Add(this.Help_Documentation_Button);
            this.Help_Panel.Controls.Add(this.Help_Walkthrough_Button);
            this.Help_Panel.Controls.Add(this.Help_Back_Button);
            this.Help_Panel.Controls.Add(this.Help_Label);
            this.Help_Panel.Location = new System.Drawing.Point(-2, 42);
            this.Help_Panel.Name = "Help_Panel";
            this.Help_Panel.Size = new System.Drawing.Size(780, 500);
            this.Help_Panel.TabIndex = 7;
            // 
            // Help_Information_Label
            // 
            this.Help_Information_Label.AutoSize = true;
            this.Help_Information_Label.Location = new System.Drawing.Point(170, 77);
            this.Help_Information_Label.Name = "Help_Information_Label";
            this.Help_Information_Label.Size = new System.Drawing.Size(472, 100);
            this.Help_Information_Label.TabIndex = 0;
            this.Help_Information_Label.Text = resources.GetString("Help_Information_Label.Text");
            this.Help_Information_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Help_Documentation_Button
            // 
            this.Help_Documentation_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Help_Documentation_Button.Location = new System.Drawing.Point(280, 295);
            this.Help_Documentation_Button.Name = "Help_Documentation_Button";
            this.Help_Documentation_Button.Size = new System.Drawing.Size(220, 69);
            this.Help_Documentation_Button.TabIndex = 1;
            this.Help_Documentation_Button.Text = "Documentation";
            this.Help_Documentation_Button.UseVisualStyleBackColor = true;
            this.Help_Documentation_Button.Click += new System.EventHandler(this.Help_Documentation_Button_Click);
            // 
            // Help_Walkthrough_Button
            // 
            this.Help_Walkthrough_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Help_Walkthrough_Button.Location = new System.Drawing.Point(280, 205);
            this.Help_Walkthrough_Button.Name = "Help_Walkthrough_Button";
            this.Help_Walkthrough_Button.Size = new System.Drawing.Size(220, 69);
            this.Help_Walkthrough_Button.TabIndex = 0;
            this.Help_Walkthrough_Button.Text = "Walkthrough";
            this.Help_Walkthrough_Button.UseVisualStyleBackColor = true;
            this.Help_Walkthrough_Button.Click += new System.EventHandler(this.Help_Walkthrough_Button_Click);
            // 
            // Help_Back_Button
            // 
            this.Help_Back_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Help_Back_Button.Location = new System.Drawing.Point(312, 449);
            this.Help_Back_Button.Name = "Help_Back_Button";
            this.Help_Back_Button.Size = new System.Drawing.Size(150, 40);
            this.Help_Back_Button.TabIndex = 2;
            this.Help_Back_Button.Text = "Back";
            this.Help_Back_Button.UseVisualStyleBackColor = true;
            this.Help_Back_Button.Click += new System.EventHandler(this.Help_Back_Button_Click);
            // 
            // Help_Label
            // 
            this.Help_Label.AutoSize = true;
            this.Help_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Help_Label.Location = new System.Drawing.Point(351, 14);
            this.Help_Label.Name = "Help_Label";
            this.Help_Label.Size = new System.Drawing.Size(82, 37);
            this.Help_Label.TabIndex = 0;
            this.Help_Label.Text = "Help";
            this.Help_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ElectoralVotingApp_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 535);
            this.Controls.Add(this.Program_MenuStrip);
            this.Controls.Add(this.Credits_Panel);
            this.Controls.Add(this.Setup_Panel);
            this.Controls.Add(this.Home_Panel);
            this.Controls.Add(this.SpreadSheet_Panel);
            this.Controls.Add(this.Statistics_Panel);
            this.Controls.Add(this.Voting_Panel);
            this.Controls.Add(this.About_Panel);
            this.Controls.Add(this.Help_Panel);
            this.MainMenuStrip = this.Program_MenuStrip;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(798, 591);
            this.MinimumSize = new System.Drawing.Size(798, 591);
            this.Name = "ElectoralVotingApp_Form";
            this.Text = "Electoral Voting App";
            this.Home_Panel.ResumeLayout(false);
            this.Home_Panel.PerformLayout();
            this.Program_MenuStrip.ResumeLayout(false);
            this.Program_MenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.Setup_Panel.ResumeLayout(false);
            this.Setup_Panel.PerformLayout();
            this.Voting_Panel.ResumeLayout(false);
            this.Voting_Panel.PerformLayout();
            this.SpreadSheet_Panel.ResumeLayout(false);
            this.SpreadSheet_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpreadSheet_DataGridView)).EndInit();
            this.Statistics_Panel.ResumeLayout(false);
            this.Statistics_Panel.PerformLayout();
            this.About_Panel.ResumeLayout(false);
            this.About_Panel.PerformLayout();
            this.About_Information_Panel.ResumeLayout(false);
            this.About_Information_Panel.PerformLayout();
            this.Credits_Panel.ResumeLayout(false);
            this.Credits_Panel.PerformLayout();
            this.Credits_Information_Panel.ResumeLayout(false);
            this.Credits_Information_Panel.PerformLayout();
            this.Help_Panel.ResumeLayout(false);
            this.Help_Panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Panel Home_Panel;
        public System.Windows.Forms.Label Home_Label;
        public System.Windows.Forms.MenuStrip Program_MenuStrip;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_File;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Start_Button;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Load_Button;
        public System.Windows.Forms.BindingSource bindingSource1;
        public System.Windows.Forms.Button Home_Help_Button;
        public System.Windows.Forms.Button Home_Load_Button;
        public System.Windows.Forms.Button Home_Start_Button;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Exit_Button;
        public System.Windows.Forms.Button Home_Credits_Button;
        public System.Windows.Forms.Button Home_About_Button;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_About;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_About_Button;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Credits_Button;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Help;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Walkthrough_Button;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Documentation_Button;
        public System.Windows.Forms.ToolStripMenuItem MenuStrip_Help_Button;
        public System.Windows.Forms.Button Home_Exit_Button;
        public System.Windows.Forms.Panel Setup_Panel;
        public System.Windows.Forms.Label Setup_CandidateCount_Label;
        public System.Windows.Forms.Button Setup_Back_Button;
        public System.Windows.Forms.Button Setup_Next_Button;
        public System.Windows.Forms.Button Setup_Create_Button;
        public System.Windows.Forms.Label Setup_Label;
        public System.Windows.Forms.TextBox Setup_CandidateCount_Textbox;
        public System.Windows.Forms.Panel Setup_CandidateInformation_Scrollbox;
        public System.Windows.Forms.Panel Voting_Panel;
        public System.Windows.Forms.Button Voting_Clear_Button;
        public System.Windows.Forms.Panel Voting_CandidatePreference_Scrollbox;
        public System.Windows.Forms.Button Voting_Back_Button;
        public System.Windows.Forms.Button Voting_Submit_Button;
        public System.Windows.Forms.Label Voting_Label;
        public System.Windows.Forms.Panel SpreadSheet_Panel;
        public System.Windows.Forms.Label SpreadSheet_CandidateCount_Text;
        public System.Windows.Forms.Label SpreadSheet_TotalVotes_Label;
        public System.Windows.Forms.Label SpreadSheet_InvalidVotes_Label;
        public System.Windows.Forms.Label SpreadSheet_CandidateCount_Label;
        public System.Windows.Forms.DataGridView SpreadSheet_DataGridView;
        public System.Windows.Forms.Button SpreadSheet_GetStatistics_Button;
        public System.Windows.Forms.Button SpreadSheet_RemoveVote_Button;
        public System.Windows.Forms.Button SpreadSheet_EditVote_Button;
        public System.Windows.Forms.Button SpreadSheet_AddVote_Button;
        public System.Windows.Forms.Button SpreadSheet_Load_Button;
        public System.Windows.Forms.Button SpreadSheet_ChangeCandidates_Button;
        public System.Windows.Forms.Button SpreadSheet_Save_Button;
        public System.Windows.Forms.Button SpreadSheet_Finish_Button;
        public System.Windows.Forms.Label SpreadSheet_Label;
        public System.Windows.Forms.Label SpreadSheet_InvalidVotes_Text;
        public System.Windows.Forms.Label SpreadSheet_TotalVotes_Text;
        public System.Windows.Forms.Panel Statistics_Panel;
        public System.Windows.Forms.TabControl Statistics_TabPanel;
        public System.Windows.Forms.Button Statistics_Back_Button;
        public System.Windows.Forms.Label Statistics_Label;
        public System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.Windows.Forms.Panel About_Panel;
        public System.Windows.Forms.Panel About_Information_Panel;
        public System.Windows.Forms.Button About_Back_Button;
        public System.Windows.Forms.Label About_Label;
        public System.Windows.Forms.Panel Credits_Panel;
        public System.Windows.Forms.Panel Credits_Information_Panel;
        public System.Windows.Forms.Button Credits_Back_Button;
        public System.Windows.Forms.Label Credits_Label;
        public System.Windows.Forms.Panel Help_Panel;
        public System.Windows.Forms.Button Help_Documentation_Button;
        public System.Windows.Forms.Button Help_Walkthrough_Button;
        public System.Windows.Forms.Button Help_Back_Button;
        public System.Windows.Forms.Label Help_Label;
        public System.Windows.Forms.Label About_Information_Label;
        public System.Windows.Forms.Label Credits_Information_Label;
        public System.Windows.Forms.Label Help_Information_Label;
        public System.Windows.Forms.Button Setup_Clear_Button;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
    }
}

