﻿/*
 * File Name : VoteData.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 4:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:59 am 19/06/2017
 */

using System;
using ElectoralVotingApp.Controllers;
using System.Windows.Forms;
using ElectoralVotingApp.DataStructures;
using System.Diagnostics;
using System.IO;

namespace ElectoralVotingApp
{
    public partial class ElectoralVotingApp_Form : Form
    {
        #region - Variables.
        public Controller controller;

        public Panel currentPanel;
        public Panel previousPanel;

        public DataGridViewRow selectedDataRow;
        #endregion


        #region - Constructor.
        public ElectoralVotingApp_Form(Controller controller)
        {
            this.controller = controller;
            
            InitializeComponent();

            currentPanel = Home_Panel;
            currentPanel.BringToFront();
        }
        #endregion


        #region - Home Panel Buttons.
        /// <summary>
        /// The events that occur when the start button is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Home_Start_Button_Click(object sender, EventArgs e)
        {
            controller.dataController = new Data_Controller(controller);

            controller.setupController.start();
            Setup_CandidateCount_Textbox.Enabled = true;
            Setup_Create_Button.Enabled = true;

            previousPanel = currentPanel;
            currentPanel = Setup_Panel;
            currentPanel.BringToFront();
        }


        /// <summary>
        /// The events that occur when the load button is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Home_Load_Button_Click(object sender, EventArgs e)
        {
            SpreadSheet_Load_Button_Click(sender, e);
        }


        /// <summary>
        /// Import a file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Home_Import_Button_Click(object sender, EventArgs e)
        {
            if (controller.loadController.openImportDialogue())
            {
                controller.spreadSheetController.start();

                // Display the spreadsheet screen.
                previousPanel = currentPanel;
                currentPanel = SpreadSheet_Panel;
                currentPanel.BringToFront();
            }
        }


        /// <summary>
        /// Opens and displays the help panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Home_Help_Button_Click(object sender, EventArgs e)
        {
            if (currentPanel != About_Panel && currentPanel != Credits_Panel && currentPanel != Help_Panel)
            {
                previousPanel = currentPanel;
            }
            currentPanel = Help_Panel;
            currentPanel.BringToFront();
        }


        /// <summary>
        /// Opens and displays the about panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Home_About_Button_Click(object sender, EventArgs e)
        {
            if (currentPanel != About_Panel && currentPanel != Credits_Panel && currentPanel != Help_Panel)
            {
                previousPanel = currentPanel;
            }
            previousPanel = currentPanel;
            currentPanel = About_Panel;
            currentPanel.BringToFront();
        }


        /// <summary>
        /// Opens and displays the credits panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Home_Credits_Button_Click(object sender, EventArgs e)
        {
            if (currentPanel != About_Panel && currentPanel != Credits_Panel && currentPanel != Help_Panel)
            {
                previousPanel = currentPanel;
            }
            previousPanel = currentPanel;
            currentPanel = Credits_Panel;
            currentPanel.BringToFront();
        }
        

        /// <summary>
        /// Exits the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Home_Exit_Button_Click(object sender, EventArgs e)
        {
            // close the program
            this.Close();
        }
        #endregion


        #region - About Panel Buttons.
        private void About_Back_Button_Click(object sender, EventArgs e)
        {
            currentPanel = previousPanel;
            previousPanel = About_Panel;
            currentPanel.BringToFront();
        }
        #endregion


        #region - Help Panel Buttons.
        private void Help_Walkthrough_Button_Click(object sender, EventArgs e)
        {
            Process.Start(Directory.GetCurrentDirectory().ToString() + "\\Documentation\\EVA-WalkThrough.docx");
        }

        private void Help_Documentation_Button_Click(object sender, EventArgs e)
        {
            Process.Start(Directory.GetCurrentDirectory().ToString() + "\\Documentation\\EVA-Documentation.docx");
        }

        private void Help_Back_Button_Click(object sender, EventArgs e)
        {
            currentPanel = previousPanel;
            previousPanel = Help_Panel;
            currentPanel.BringToFront();
        }

        #endregion


        #region - Credits Panel Buttons.
        private void Credits_Back_Button_Click(object sender, EventArgs e)
        {
            currentPanel = previousPanel;
            previousPanel = Credits_Panel;
            currentPanel.BringToFront();
        }
        
        #endregion


        #region - Setup Panel Buttons.
        private void Setup_Create_Button_Click(object sender, EventArgs e)
        {
            int candidateCount = 0;
            // Check that candidate count input is not empty and is an integer
            if (String.IsNullOrEmpty(Setup_CandidateCount_Textbox.Text))
            {
                MessageBox.Show("TextBox is empty, please add a number");
            }
            else
            {
                if(int.TryParse(Setup_CandidateCount_Textbox.Text, out candidateCount))
                {
                    // If is integer
                    controller.setupController.createCandidateFields(int.Parse(Setup_CandidateCount_Textbox.Text));
                }
                else
                {
                    MessageBox.Show("TextBox input is not an integer, please enter an integer");
                } 
            }
        }

        public void Setup_Next_Button_Click(object sender, EventArgs e)
        {
            int candidateCount = 0;

            controller.dataController.resetCandidates();

            if (int.TryParse(Setup_CandidateCount_Textbox.Text, out candidateCount))
            {
                if (candidateCount > 0)
                {
                   
                    if(controller.setupController.errorCheck() == false)
                    {
                        currentPanel = Setup_Panel;
                        previousPanel = Home_Panel;
                        currentPanel.BringToFront();
                        
                    }
                    else
                    {
                        // Create the candidates in the data controller.
                        controller.setupController.createCandidates();

                        // Display the spreadsheet screen.
                        previousPanel = currentPanel;
                        currentPanel = SpreadSheet_Panel;
                        currentPanel.BringToFront();
                    }
                    
                }
                else
                {
                    var invalData = MessageBox.Show("Invalid data");
                    if (invalData == DialogResult.OK)
                    {
                        currentPanel = Setup_Panel;
                        previousPanel = Home_Panel;
                        currentPanel.BringToFront();
                    }
                    // Otherwise there were no candidates created, can't move on to the spreadsheet screen.
                }

                
            }
        }


        private void Setup_Clear_Button_Click(object sender, EventArgs e)
        {
            controller.setupController.clearData();
        }

        private void Setup_Back_Button_Click(object sender, EventArgs e)
        {
            if (previousPanel != SpreadSheet_Panel)
            {
                currentPanel = previousPanel;
                previousPanel = Setup_Panel;
                currentPanel.BringToFront();
            }
            else
            {
                controller.spreadSheetController.start();

                currentPanel = previousPanel;
                previousPanel = Setup_Panel;
                currentPanel.BringToFront();
            }
        }

        #endregion


        #region - Voting Panel Buttons.
        private void Voting_Clear_Button_Click(object sender, EventArgs e)
        {
            controller.votingController.clearData();
        }

        private void Voting_Back_Button_Click(object sender, EventArgs e)
        {
            currentPanel = previousPanel;
            previousPanel = Voting_Panel;
            currentPanel.BringToFront();
        }

        private void Voting_Submit_Button_Click(object sender, EventArgs e)
        {
            if(controller.votingController.errorCheck() == false)
            {
                previousPanel = SpreadSheet_Panel;
                currentPanel = Voting_Panel;
                currentPanel.BringToFront();
            }
            else if (controller.votingController.submitVote())
            {
                controller.spreadSheetController.start();

                previousPanel = currentPanel;
                currentPanel = SpreadSheet_Panel;
                currentPanel.BringToFront();
            }
        }
        #endregion


        #region - Spreadsheet Panel Buttons.
        private void SpreadSheet_ChangeCandidates_Button_Click(object sender, EventArgs e)
        {
            controller.setupController.editingCandidates = true;

            Setup_CandidateCount_Textbox.Enabled = false;
            Setup_Create_Button.Enabled = false;

            Setup_Panel.BringToFront();
            currentPanel = Setup_Panel;
            previousPanel = SpreadSheet_Panel;
        }

        #region - Add/Edit/Remove methods.
        private void SpreadSheet_AddVote_Button_Click(object sender, EventArgs e)
        {
            controller.votingController.editingVote = false;
            controller.votingController.createVotingFields();

            previousPanel = currentPanel;
            currentPanel = Voting_Panel;
            currentPanel.BringToFront();
        }

        private void SpreadSheet_EditVote_Button_Click(object sender, EventArgs e)
        {
            if (selectedDataRow != null)
            {
                controller.votingController.createVotingFields();

                controller.votingController.editingVote = true;

                int[] preferences = new int[selectedDataRow.Cells.Count];
                string[] candidates = new string[selectedDataRow.Cells.Count];

                for (int i = 0; i < selectedDataRow.Cells.Count; i++)
                {
                    controller.votingController.candidatePreferenceTextBoxes[i].Text = selectedDataRow.Cells[i].Value.ToString();

                    candidates[i] = controller.dataController.candidates[i].candidateName;
                    int.TryParse(selectedDataRow.Cells[i].Value.ToString(), out preferences[i]);
                }

                controller.votingController.previousVote = new VoteData(candidates, preferences);

                Voting_Panel.BringToFront();
                currentPanel = Voting_Panel;
                previousPanel = SpreadSheet_Panel;
            }
            else
            {
                // No vote was selected, display a prompt to the user and cancel this operation.
            }
        }

        private void SpreadSheet_RemoveVote_Button_Click(object sender, EventArgs e)
        {
            if (selectedDataRow != null)
            {
                int[] preferences = new int[selectedDataRow.Cells.Count];
                string[] candidates = new string[selectedDataRow.Cells.Count];

                for (int i = 0; i < selectedDataRow.Cells.Count; i++)
                {
                    candidates[i] = controller.dataController.candidates[i].candidateName;
                    int.TryParse(selectedDataRow.Cells[i].Value.ToString(), out preferences[i]);
                }

                controller.dataController.removeVote(new VoteData(candidates, preferences));

                controller.spreadSheetController.start();
            }
        }
        #endregion


        #region - Save/Load methods.
        private void SpreadSheet_Save_Button_Click(object sender, EventArgs e)
        {
            controller.saveController.openSaveDialogue();
        }

        private void SpreadSheet_Load_Button_Click(object sender, EventArgs e)
        {
            if (controller.loadController.openLoadDialogue())
            {
                controller.spreadSheetController.start();

                // Display the spreadsheet screen.
                previousPanel = currentPanel;
                currentPanel = SpreadSheet_Panel;
                currentPanel.BringToFront();
            }
        }
        #endregion


        private void SpreadSheet_GetStatistics_Button_Click(object sender, EventArgs e)
        {
            controller.statisticsController.produceStatistics();

            previousPanel = currentPanel;
            currentPanel = Statistics_Panel;
            currentPanel.BringToFront();
        }

        private void SpreadSheet_Finish_Button_Click(object sender, EventArgs e)
        {
            // Unsaved data check here


            controller.dataController.resetCandidates();

            controller.dataController.candidatesSet = false;

            previousPanel = currentPanel;
            currentPanel = Home_Panel;
            currentPanel.BringToFront();
        }

        #endregion


        #region - Statistics Panel Buttons.
        private void Statistics_Back_Button_Click(object sender, EventArgs e)
        {
            currentPanel = previousPanel;
            previousPanel = SpreadSheet_Panel;
            currentPanel.BringToFront();
        }

        #endregion
        

        #region - Data Grid View Methods.
        private void DataGridView_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || string.IsNullOrEmpty(SpreadSheet_DataGridView.Rows[e.RowIndex].Cells[0].Value.ToString()))
            {
                selectedDataRow = null;
            }
            else
            {
                selectedDataRow = SpreadSheet_DataGridView.Rows[e.RowIndex];
            }
        }
        #endregion
    }
}
