﻿/*
 * File Name : VoteRoundData.cs 
 * 
 * Author : Tristyn Mackay
 * 
 * Date Created : 4:00 pm 13/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:59 am 19/06/2017
 */

using System;
using System.Collections.Generic;

namespace ElectoralVotingApp.DataStructures
{
    public class VoteRoundData
    {
        #region - Variables.
        public int roundNumber = 1;
        public VoteRoundData nextRound;

        public CandidateData[] candidates;
        public VoteData[] candidateVotes;
        public int[] candidateVoteCount;

        public List<CandidateData> winningCandidates;

        public CandidateData[] precludedCandidates;
        #endregion


        #region - Constructors
        public VoteRoundData(CandidateData[] candidates, VoteData[] candidateVotes)
        {
            this.candidates = candidates;
            this.candidateVotes = candidateVotes;

            calculateRoundData();
        }


        public VoteRoundData(CandidateData[] candidates, VoteData[] candidateVotes, CandidateData[] precludedCandidates, int roundNumber)
        {
            this.candidates = candidates;
            this.candidateVotes = candidateVotes;
            this.precludedCandidates = precludedCandidates;
            this.roundNumber = roundNumber;

            calculateRoundData();
        }
        #endregion


        #region - Calculate Round Data.
        /// <summary>
        /// calculate the round data
        /// </summary>
        private void calculateRoundData()
        {
            candidateVoteCount = new int[candidates.Length];

            // Go through each vote.
            for (int i = 0; i < candidateVotes.Length; i++)
            {
                int voteIndex = 0;
                int candidateIndex = 0;
                // Go through each preference until the desired preference is found.
                for (int k = 0; k < candidateVotes[i].candidatePreferences.Length; k++)
                {
                    // Check if the current current candidate preference is allowd in this round.
                    for (int j = 0; j < candidates.Length; j++)
                    {
                        if (candidates[j].candidateName.Equals(candidateVotes[i].candidateNames[k])) 
                        {
                            if (candidateVotes[i].candidatePreferences[k] < candidateVotes[i].candidatePreferences[voteIndex])
                            {
                                voteIndex = k;
                                candidateIndex = j;
                            }
                        }
                    }
                }
                // Add one to the candidate's vote count.
                candidateVoteCount[candidateIndex]++;
            }


            winningCandidates = new List<CandidateData>();

            // Find the winning candidate(s).
            for (int i = 0; i < candidateVoteCount.Length; i++)
            {
                if (candidateVoteCount[i] > candidateVotes.Length / 2)
                {
                    winningCandidates.Add(candidates[i]);
                    break;
                }
                else if (candidateVoteCount[i] == candidateVotes.Length / 2) {
                        winningCandidates.Add(candidates[i]);
                }
            }


            // If no winning candidate(s) was found then create a new round.
            if (winningCandidates == null || winningCandidates.Count == 0)
            {
                List<CandidateData> lowestVoteCandidates = new List<CandidateData>();

                int lowestVoteCount = 0;

                // Find the lowest vote candidae(s).
                for (int i = 0; i < candidateVoteCount.Length; i++)
                {
                    // By default the first candidate must set the lowestVoteCount.
                    if (i == 0)
                    {
                        lowestVoteCount = candidateVoteCount[i];
                        lowestVoteCandidates.Add(candidates[i]);
                    }
                    else
                    {
                        // If this candidate has a lower count, we recreate the list and add this candidate to it.
                        if (candidateVoteCount[i] < lowestVoteCount)
                        {
                            lowestVoteCount = candidateVoteCount[i];
                            lowestVoteCandidates = new List<CandidateData>();
                            lowestVoteCandidates.Add(candidates[i]);
                        }
                        else if (candidateVoteCount[i] == lowestVoteCount)
                        {
                            // If this candidate has the same amount of votes as the lowestVoteCount,
                            // then we also add this candidate to the list.
                            lowestVoteCandidates.Add(candidates[i]);
                        }
                    }
                }

                // For this algorithm we are randomly choosing from all of the lowest vote candidates.
                Random rand = new Random();
                int precludedCandidateIndex = rand.Next(0, lowestVoteCandidates.Count);
                
                
                CandidateData[] precludableCandidates = new CandidateData[1];
                precludableCandidates[0] = lowestVoteCandidates[precludedCandidateIndex];


                CandidateData[] newCandidates = new CandidateData[candidates.Length - 1];
                // Identify all but the precluded candidate and add them to the new candidates list.
                int candidateIndex = 0;
                for (int i = 0; i < candidates.Length; i++)
                {
                    if (candidates[i].candidateName.Equals(precludableCandidates[0].candidateName) == false)
                    {
                        newCandidates[candidateIndex] = candidates[i];
                        candidateIndex++;
                    }
                }
                
                // Create the next set of round data with the new information that has been gathered.
                nextRound = new VoteRoundData(newCandidates, candidateVotes, precludableCandidates, roundNumber + 1);
            }
        }
        #endregion
    }
}
