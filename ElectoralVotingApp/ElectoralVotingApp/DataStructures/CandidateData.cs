﻿/*
 * File Name : CandidateData.cs 
 * 
 * Author : Tristyn Mackay
 * 
 * Date Created : 4:00 pm 13/06/2017 
 * 
 * Last Edit Author : Tristyn Mackay
 * 
 * Last Edit Date : 13/06/2017
 */

using System;

namespace ElectoralVotingApp.DataStructures
{
    public class CandidateData
    {
        public string candidateName = "";


        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="candidateName"></param>
        public CandidateData (string candidateName)
        {
            this.candidateName = candidateName;
        }


        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="candidate"></param>
        public CandidateData (CandidateData candidate)
        {
            candidateName = candidate.candidateName;
        }
    }
}
