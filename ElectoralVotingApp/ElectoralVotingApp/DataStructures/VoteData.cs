﻿/*
 * File Name : VoteData.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 4:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:59 am 19/06/2017
 */

using System;

namespace ElectoralVotingApp.DataStructures
{
    public class VoteData
    {
        #region - Variables.
        public string[] candidateNames;
        public int[] candidatePreferences;
        #endregion


        #region - Constructor.
        public VoteData(string[] candidateNames, int[] candidatePreferences)
        {
            this.candidateNames = candidateNames;
            this.candidatePreferences = candidatePreferences;
        }
        #endregion

        /// <summary>
        /// Check if another vote is the same as this vote.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(VoteData other)
        {
            // Check if the candidate preferences are the same.
            for (int i = 0; i < candidatePreferences.Length; i++)
            {
                if (i < other.candidatePreferences.Length && 
                    candidatePreferences[i] != other.candidatePreferences[i])
                {
                    return false;
                }
            }

            // Now check if the candidate names are the same.
            // (We do this second as string comparison takes longer to perform)
            for (int i = 0; i < candidateNames.Length; i++)
            {
                if (i < other.candidateNames.Length &&
                    candidateNames[i] != other.candidateNames[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
