﻿/*
 * File Name : Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:58 am 19/06/2017
 */

using System;
using System.Windows.Forms;

namespace ElectoralVotingApp.Controllers
{
    public class Controller
    {
        #region - Variables.
        // Program form.
        public ElectoralVotingApp_Form EVForm; 

        // General controllers.
        public Home_Controller homeController;
        public About_Controller aboutController;
        public Credits_Controller creditController;
        public Help_Controller helpController;

        // Data view controllers.
        public Setup_Controller setupController;
        public Voting_Controller votingController;
        public SpreadSheet_Controller spreadSheetController;
        public Statistics_Controller statisticsController;

        // Data controllers.
        public Data_Controller dataController;
        public Save_Controller saveController;
        public Load_Controller loadController;

        #endregion


        #region - Constructor.
        public Controller()
        {
            homeController = new Home_Controller(this);
            aboutController = new About_Controller(this);
            creditController = new Credits_Controller(this);
            helpController = new Help_Controller(this);

            setupController = new Setup_Controller(this);
            votingController = new Voting_Controller(this);
            spreadSheetController = new SpreadSheet_Controller(this);
            statisticsController = new Statistics_Controller(this);

            dataController = new Data_Controller(this);
            saveController = new Save_Controller(this);
            loadController = new Load_Controller(this);

            EVForm = new ElectoralVotingApp_Form(this);

            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(EVForm);

            showHomePanel();
        }
        #endregion


        #region - Panel Methods.
        // show all the panels in the form
        public void showHomePanel()
        {
            EVForm.Home_Panel.Visible = true;
        }
        
        public void showAboutPanel()
        { 
            EVForm.About_Panel.Visible = true;
        }

        public void showCreditsPanel()
        {
            EVForm.Credits_Panel.Visible = true;
        }

        public void showHelpPanel()
        {
            EVForm.Help_Panel.Visible = true;
        }

        public void showSetupPanel()
        {
            EVForm.Setup_Panel.Visible = true;
        }
        
        public void showVotingPanel()
        {
            EVForm.Voting_Panel.Visible = true;
        }

        public void showSpreadSheetPanel()
        {
            EVForm.SpreadSheet_Panel.Visible = true;
        }

        public void showStatisticsPanel()
        {
            EVForm.Statistics_Panel.Visible = true;
        }
        #endregion
    }
}
