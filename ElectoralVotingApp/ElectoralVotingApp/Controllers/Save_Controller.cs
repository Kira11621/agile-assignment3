﻿/*
 * File Name : Save_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:58 am 19/06/2017
 */

using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace ElectoralVotingApp.Controllers
{
    public class Save_Controller
    {
        #region - Variables.
        private Controller controller;
        #endregion


        #region - Constructor.
        public Save_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion


        #region - Save Methods.
        /// <summary>
        /// open the save dialogue
        /// </summary>
        /// <returns></returns>
        public bool openSaveDialogue()
        {
            SaveFileDialog saveDialog = new SaveFileDialog();

            string path = Directory.GetCurrentDirectory().ToString() + "\\Saves";

            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
            saveDialog.InitialDirectory = path;

            saveDialog.Filter = "EVA Data (*.EVAD)|*.evad";

            saveDialog.Title = "Save Electoral Vote Application Data";

            saveDialog.ShowDialog();

            // Check if the file name is true before saving.
            if (String.IsNullOrWhiteSpace(saveDialog.FileName) == false)
            {
                // Check if the file name already exists.
                if (saveDialog.CheckFileExists == false)
                {
                    Data_Controller dataController = controller.dataController;
                    if (saveFile(saveDialog, dataController.getDataTable()) == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show(controller.EVForm, "File Already Exists!", "Failed To Save File!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            return false;
        }
        #endregion

        /// <summary>
        /// save the file
        /// </summary>
        /// <param name="saveDialog"></param>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public bool saveFile(SaveFileDialog saveDialog, DataTable dataTable) {
            // Save the file as a CSV to start with.
            CreateCSVData(dataTable, saveDialog.FileName);

            return true;
        }

        #region - Create CSV Data.
        /// <summary>
        /// Create the CSV data
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="filePath"></param>
        private void CreateCSVData(DataTable dataTable, string filePath) {
            try
            {
                // Create the CSV file to which grid data will be exported.
                StreamWriter streamWriter = new StreamWriter(filePath, true);
                // First we will write the headers.
                int columnCount = dataTable.Columns.Count;
                for (int i = 0; i < columnCount; i++)
                {
                    streamWriter.Write(dataTable.Columns[i]);
                    if (i < columnCount - 1)
                    {
                        streamWriter.Write(",");
                    }
                }
                streamWriter.Write(streamWriter.NewLine);

                // Now write all the rows.
                for (int j = 0; j < dataTable.Rows.Count; j++)
                {
                    for (int i = 0; i < columnCount; i++)
                    {
                        string columnString = dataTable.Rows[j].ItemArray[i].ToString();
                        // Search the string And replace the Quotation Marks.
                        for (int k = 0; k < columnString.Length; k++)
                        {
                            if (columnString[k].Equals("\""))
                            {
                                Console.WriteLine(columnString[k]);
                                string newString = columnString.Substring(0, k);
                                newString += "\"\"";
                                if (k + 1 < columnString.Length)
                                {
                                    newString += columnString.Substring(k + 1);
                                }
                                else
                                {
                                    break;
                                }
                                columnString = newString;
                                Console.WriteLine(newString);
                                k += 2;
                            }
                        }

                        // Add Quotation marks to the start And end of the columnString to 
                        // sanitise the data for Excel to read properly.
                        string tempString = "\"";
                        tempString += columnString + "\"";
                        columnString = tempString;

                        if (Convert.IsDBNull(dataTable.Rows[j].ItemArray[i]) == false)
                        {
                            streamWriter.Write(columnString);
                        }
                        if (i < columnCount - 1)
                        {
                            streamWriter.Write(",");
                        }
                    }
                    streamWriter.Write(streamWriter.NewLine);
                }
                streamWriter.Close();
            } catch (Exception ex) {
                throw ex;
            }
        }
        #endregion
    }
}
