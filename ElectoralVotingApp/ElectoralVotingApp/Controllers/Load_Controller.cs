﻿/*
 * File Name : Load_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:58 am 16/06/2017
 */

using ElectoralVotingApp.DataStructures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace ElectoralVotingApp.Controllers
{
    public class Load_Controller
    {
        #region - Variables.
        private Controller controller;
        #endregion


        #region - Constructors.
        public Load_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion


        #region - Load methods
        /// <summary>
        /// open the Load file dialogue
        /// </summary>
        /// <returns></returns>
        public bool openLoadDialogue() {

            OpenFileDialog openDialog = new OpenFileDialog();

            string path = Directory.GetCurrentDirectory().ToString() + "\\Saves";

            if (Directory.Exists(path) == false) {
                Directory.CreateDirectory(path);
            }
            openDialog.InitialDirectory = path;

            openDialog.Filter = "EVA Data (*.evad)|*.evad";

            openDialog.Title = "Load EVA set Data";

            openDialog.ShowDialog();

            // Check if the file name is true before saving.
            if (string.IsNullOrWhiteSpace(openDialog.FileName) == false) {
                // Check if a file was selected and if the file name already exists.
                if (openDialog.CheckFileExists == true) {
                    loadFile(openDialog);
                    return true;
                }
                else {
                    MessageBox.Show(controller.EVForm, "File Doesn't Exist!", "Failed To Open File!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            return false;
        }

        #region - Import Dialogue.
        /// <summary>
        /// open the import file dialogue
        /// </summary>
        /// <returns></returns>
        public bool openImportDialogue()
        {

            OpenFileDialog openDialog = new OpenFileDialog();

            string path = Directory.GetCurrentDirectory().ToString() + "\\Saves";

            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
            openDialog.InitialDirectory = path;

            openDialog.Title = "Load EVA set Data";

            openDialog.ShowDialog();

            // Check if the file name is true before saving.
            if (string.IsNullOrWhiteSpace(openDialog.FileName) == false)
            {
                // Check if a file was selected and if the file name already exists.
                if (openDialog.CheckFileExists == true)
                {
                    loadFile(openDialog);
                    return true;
                }
                else
                {
                    MessageBox.Show(controller.EVForm, "File Doesn't Exist!", "Failed To Open File!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            return false;
        }
        #endregion

        /// <summary>
        /// Load the file
        /// </summary>
        /// <param name="openDialog"></param>
        /// <returns></returns>
        public bool loadFile(OpenFileDialog openDialog) {
            //dataController.setDataSaved(True);

            ReadData(openDialog.FileName);
            
            return true;
        }

        #region - Read Data.
        /// <summary>
        /// Read the data in the file
        /// </summary>
        /// <param name="path"></param>
        private void ReadData(string path) {
            try {
                StreamReader streamReader = new StreamReader(path);

                string[] candidateNames = new string[0];

                int index = 0;

                while (streamReader.EndOfStream == false)
                {
                    string line = streamReader.ReadLine();

                    if (string.IsNullOrEmpty(line) == false)
                    {
                        string[] columns = line.Split(',');

                        if (index == 0)
                        {
                            controller.dataController = new Data_Controller(controller);

                            candidateNames = new string[columns.Length];

                            for (int i = 0; i < columns.Length; i++)
                            {
                                string result = columns[i];
                                if (result.Contains("'"))
                                {
                                    result = result.Substring(1);
                                    result = result.Remove(result.Length - 1);
                                }
                                CandidateData candidate = new CandidateData(result);

                                candidateNames[i] = candidate.candidateName;
                                controller.dataController.candidates.Add(candidate);
                            }
                        }
                        else
                        {
                            int[] preferences = new int[columns.Length];

                            for (int i = 0; i < preferences.Length; i++)
                            {
                                string result = columns[i];
                                if (result.Contains("\""))
                                {
                                    result = result.Substring(1);
                                    result = result.Remove(result.Length - 1);
                                }
                                int.TryParse(result, out preferences[i]);
                            }

                            controller.dataController.addVote(new VoteData(candidateNames, preferences));
                        }
                    }
                    index++;
                }

                /*
                StreamReader streamReader = new StreamReader(path);

                int index = 0;
                while (streamReader.Peek() >= 0) {
                    string dataString = "";

                    dataString = streamReader.ReadLine();

                    // If the string is empty, skip this line.
                    if (String.IsNullOrEmpty(dataString) == false) {

                        int startIndex = -1;
                        int endIndex = 0;

                        endIndex -= 3;
                        for (int k = 0; k <= 4; k++) {
                            Console.WriteLine("This is the value - " + dataString[3].ToString());
                            startIndex = endIndex + 4;
                            for (int i = startIndex; i <= dataString.Length; i--) {
                                if (dataString[i].ToString().Equals(",")) {
                                    endIndex = i - 2;
                                    break;
                                } else if (i == dataString.Length - 1) {
                                    endIndex = i - 1;
                                }
                            }
                            if (startIndex >= 0 && endIndex >= 0) {
                                string tempString = dataString.Substring(startIndex, endIndex - startIndex + 1);

                                // Setup the vote.
                                if (k == 0) {
                                    vote.setPartName(tempString);
                                }
                                else if (k == 1) {
                                    part.setWeight(Convert.ToDouble(tempString));
                                }
                                else if (k == 2) {
                                    part.setCost(Convert.ToDouble(tempString))
                                }
                                else if (k == 3) {
                                    part.setQuantity(Integer.Parse(tempString));
                                }
                                else if (k == 4) {
                                    part.setDescription(tempString)
                                }
                            }

                            dataController.addPart(part);
                        }
                    }
                }

                // Add 1 to the line count.
                index += 1;
                */
            } catch {
            }
        }
        #endregion

        #endregion
    }
}
