﻿/*
 * File Name : Data_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Tristyn Mackay
 * 
 * Last Edit Date : 16/06/2017
 */

using ElectoralVotingApp.DataStructures;
using System;
using System.Collections.Generic;
using System.Data;

namespace ElectoralVotingApp.Controllers
{
    public class Data_Controller
    {
        #region - Variables.
        public Controller controller;

        public List<CandidateData> candidates;
        public bool candidatesSet = false;

        public List<VoteRoundData> roundData;

        public List<VoteData> voteData;
        public List<VoteData> invalidVotes;
        public List<VoteData> validVotes;
        #endregion


        #region - Constructors.
        /// <summary>
        /// Default Constructor.
        /// </summary>
        /// <param name="controller"></param>
        public Data_Controller(Controller controller)
        {
            this.controller = controller;

            candidates = new List<CandidateData>();

            roundData = new List<VoteRoundData>();

            voteData = new List<VoteData>();
            validVotes = new List<VoteData>();
            invalidVotes = new List<VoteData>();
        }

        /// <summary>
        /// Copy Constructor.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="dataController"></param>
        public Data_Controller(Controller controller, Data_Controller dataController)
        {
            this.controller = controller;

            candidates = dataController.candidates;

            roundData = dataController.roundData;

            voteData = dataController.voteData;
            invalidVotes = dataController.invalidVotes;
            validVotes = dataController.validVotes;
        }
        #endregion


        #region - Data Methods.
        /// <summary>
        /// Add a candidate to the candidates list.
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        public bool addCandidate(CandidateData candidate)
        {
            candidates.Add(candidate);

            // If this candidate addition is a change from a previous list then re-produce vote data.
            if (candidatesSet == true)
            {
                List<VoteData> newVoteData = new List<VoteData>();
                List<VoteData> newValidVotes = new List<VoteData>();
                List<VoteData> newInvalidVotes = new List<VoteData>();

                foreach (VoteData vote in voteData)
                {
                    newVoteData.Add(vote);
                    if (checkVoteValid(vote))
                    {
                        newValidVotes.Add(vote);
                    }
                    else
                    {
                        newInvalidVotes.Add(vote);
                    }
                }

                voteData = newVoteData;
                validVotes = newValidVotes;
                invalidVotes = newInvalidVotes;
            }

            return true;
        }


        /// <summary>
        /// Reset the candidates list to start from anew.
        /// </summary>
        public void resetCandidates()
        {
            candidates = new List<CandidateData>();
        }


        /// <summary>
        /// Add a vote to the vote data.
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
        public bool addVote(VoteData vote)
        {
            // Check if the vote is valid.
            if (checkVoteValid(vote))
            {
                // If the vote is valid we will add it to the voteData array,
                // and add it to the validVotes array.
                voteData.Add(vote);
                validVotes.Add(vote);
            }
            else
            {
                // If the vote is invalid we will add it to the voteData array,
                // but add it to the invalidVotes array as well.
                voteData.Add(vote);
                invalidVotes.Add(vote);
            }

            return true;
        }


        /// <summary>
        /// Remove a vote from the vote data.
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
        public bool removeVote(VoteData vote)
        {
            // Check if the vote is in the voteData array.
            for (int i = 0; i < voteData.Count; i++)
            {
                if (voteData[i].Equals(vote))
                {
                    voteData.RemoveAt(i);
                    break;
                }
            }

            // Check if the vote is in the validVotes array.
            for (int i = 0; i < validVotes.Count; i++)
            {
                if (validVotes[i].Equals(vote))
                {
                    validVotes.RemoveAt(i);
                    return true;
                }
            }


            // Check if the vote is in the invalidVotes array.
            for (int i = 0; i < invalidVotes.Count; i++)
            {
                if (invalidVotes[i].Equals(vote))
                {
                    invalidVotes.RemoveAt(i);
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Change the values of a vote in the vote data.
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
        public bool changeVote(VoteData previousVote, VoteData newVote)
        {
            // Check if the vote is in the voteData array.
            for (int i = 0; i < voteData.Count; i++)
            {
                if (voteData[i].Equals(previousVote))
                {
                    voteData[i] = newVote;
                    break;
                }
            }


            // Check if the vote is in the validVotes array.
            for (int i = 0; i < validVotes.Count; i++)
            {
                if (validVotes[i].Equals(previousVote))
                {
                    validVotes.RemoveAt(i);
                    break;
                }
            }


            // Check if the vote is in the invalidVotes array.
            for (int i = 0; i < invalidVotes.Count; i++)
            {
                if (invalidVotes[i].Equals(previousVote))
                {
                    invalidVotes.RemoveAt(i);
                }
            }


            if (checkVoteValid(newVote))
            {
                validVotes.Add(newVote);
                return true;
            }
            else
            {
                invalidVotes.Add(newVote);
                return true;
            }
        }
        #endregion


        #region - Data Viewing Methods.
        #region - Data Table methods.
        /// <summary>
        /// Get a DataTable of all the data.
        /// </summary>
        /// <returns></returns>
        public DataTable getDataTable()
        {
            DataTable dataTable = new DataTable("Votes");

            // Create the columns.
            foreach (CandidateData candidate in candidates)
            {
                dataTable.Columns.Add(candidate.candidateName, Type.GetType("System.String"));
            }

            //Populate the rows.
            foreach (VoteData vote in voteData)
            {
                DataRow newRow = dataTable.Rows.Add();

                // Populate this new row.
                for (int i = 0; i < candidates.Count; i++)
                {
                    newRow[i] = vote.candidatePreferences[i];
                }
            }

            return dataTable;
        }


        /// <summary>
        /// Get a DataTable of the valid data.
        /// </summary>
        /// <returns></returns>
        public DataTable getValidDataTable()
        {
            DataTable dataTable = new DataTable("Valid Votes");

            // Create the columns.
            foreach (CandidateData candidate in candidates) {
                dataTable.Columns.Add(candidate.candidateName, Type.GetType("System.Integer"));
            }

            //Populate the rows.
            foreach (VoteData vote in validVotes)
            {
                DataRow newRow = dataTable.Rows.Add();

                // Populate this new row.
                for (int i = 0; i < candidates.Count; i++)
                {
                    newRow[i] = vote.candidatePreferences[i];
                }
            }

            return dataTable;
        }


        /// <summary>
        /// Get a DataTable of the invalid data.
        /// </summary>
        /// <returns></returns>
        public DataTable getInvalidDataTable()
        {
            DataTable dataTable = new DataTable("Invalid Votes");

            // Create the columns.
            foreach (CandidateData candidate in candidates)
            {
                dataTable.Columns.Add(candidate.candidateName, Type.GetType("System.Integer"));
            }

            //Populate the rows.
            foreach (VoteData vote in invalidVotes)
            {
                DataRow newRow = dataTable.Rows.Add();

                // Populate this new row.
                for (int i = 0; i < candidates.Count; i++)
                {
                    newRow[i] = vote.candidatePreferences[i];
                }
            }

            return dataTable;
        }
        #endregion


        #region - Round methods.
        /// <summary>
        /// Generate a set of rounds to summarise the votes,
        /// the last round is always the final round.
        /// </summary>
        /// <returns></returns>
        public VoteRoundData[] generateRounds()
        {
            // Generate rounds 1 by 1 uing only valid votes, 
            // adding them to the roundData array as they are generated.


            return null;
        }
        #endregion
        #endregion.


        #region - Vote Validity.
        /// <summary>
        /// Check if the vote given is valid.
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
        public bool checkVoteValid (VoteData vote)
        {
            // Check if any preferences have been made.
            if (vote.candidatePreferences.Length == 0)
            {
                // No preferences made, this is an invalid vote.
                return false;
            }

            // Check if there are the same amount of candidates as the current schema.
            if (vote.candidateNames.Length == 0)
            {
                // No candidates given, this is an invalid vote.
                return false;
            }
            
            int[] usedPreferences = new int[vote.candidatePreferences.Length];
            // Check through all candidate preferences to make sure they are valid.
            for (int i = 0; i < vote.candidatePreferences.Length; i++)
            {
                // Check if the candidate preference is within the correct range.
                if (vote.candidatePreferences[i] <= 0 || 
                    vote.candidatePreferences[i] > vote.candidatePreferences.Length)
                {
                    // Preference is outside the given preference range, this is an invalid vote.
                    return false;
                }

                // Check if the candidate preference number has already been used.
                for (int k = 0; k < usedPreferences.Length; k++)
                {
                    if (vote.candidatePreferences[i] == usedPreferences[k])
                    {
                        // Number of preference has already been used in this vote, this is an invalid vote.
                        return false;
                    }
                }
            }

            return true;
        }
        #endregion
    }
}
