﻿/*
 * File Name : Help_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:54 am 19/06/2017
 */

using System;

namespace ElectoralVotingApp.Controllers
{
    public class Help_Controller
    {
        #region - Variables.
        private Controller controller;
        #endregion


        #region - Constructor.
        public Help_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion
    }
}
