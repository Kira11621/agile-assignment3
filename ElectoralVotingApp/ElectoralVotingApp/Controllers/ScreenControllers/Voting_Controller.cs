﻿/*
 * File Name : Voting_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:57 am 19/06/2017
 */

using ElectoralVotingApp.DataStructures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ElectoralVotingApp.Controllers
{
    public class Voting_Controller
    {
        #region - Variables.
        private Controller controller;

        public bool editingVote = false;
        public VoteData previousVote;

        private Label[] candidateLabels;
        public TextBox[] candidatePreferenceTextBoxes;

        public String error = "";
        public bool returnBool = true;
        #endregion


        #region - Constructor.
        public Voting_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion


        #region - Vote Data.
        /// <summary>
        /// Create the voting fields
        /// </summary>
        public void createVotingFields()
        {
            if(candidateLabels != null)
            {
                for (int i = 0; i < candidateLabels.Length; i++)
                {
                    candidateLabels[i].Dispose();
                    candidatePreferenceTextBoxes[i].Dispose();
                }
            }

            candidateLabels = new Label[controller.dataController.candidates.Count];
            candidatePreferenceTextBoxes = new TextBox[controller.dataController.candidates.Count];

            for (int i = 0; i < controller.dataController.candidates.Count; i++)
            {
                candidateLabels[i] = new Label();
                candidateLabels[i].MinimumSize = new Size(110, 20);
                candidateLabels[i].Parent = controller.EVForm.Voting_CandidatePreference_Scrollbox;
                candidateLabels[i].Location = new Point(20, 30 + (25 * (i - 1)));
                candidateLabels[i].Text = controller.dataController.candidates[i].candidateName;

                candidatePreferenceTextBoxes[i] = new TextBox();
                candidatePreferenceTextBoxes[i].Parent = controller.EVForm.Voting_CandidatePreference_Scrollbox;
                candidatePreferenceTextBoxes[i].Size = new Size(120, 26);
                candidatePreferenceTextBoxes[i].Location = new Point(130, 27 + (25 * (i - 1)));
            }
        }

        #region - Submit Vote.
        /// <summary>
        /// Attempt to submit a vote.
        /// </summary>
        /// <returns></returns>
        public bool submitVote()
        {
            if (editingVote)
            {
                return changeVote();
            }
            else
            {
                return createVote();
            }
        }
        #endregion


        #region - Change Vote.
        /// <summary>
        /// Attempt to Change a vote.
        /// </summary>
        /// <returns></returns>
        private bool changeVote()
        {
            string[] candidateNames = new string[candidateLabels.Length];
            int[] preferences;

            if (checkInputs(out preferences))
            {
                for (int i = 0; i < controller.dataController.candidates.Count; i++)
                {
                    candidateNames[i] = candidateLabels[i].Text;
                }

                if (controller.dataController.changeVote(previousVote, new VoteData(candidateNames, preferences)))
                {
                    return true;
                }
                else
                {
                    // Couldn't add the candidate data. The preference choices are either out of the given preference range 
                    // or there is a duplicate preference choice. Prompt the user and cancel.
                }
            }

            return false;
        }
        #endregion


        #region - Create Vote.
        /// <summary>
        /// Attempt to Create a vote.
        /// </summary>
        /// <returns></returns>
        private bool createVote()
        {
            string[] candidateNames = new string[candidateLabels.Length];
            int[] preferences;
            
                if (checkInputs(out preferences))
                {
                    for (int i = 0; i < controller.dataController.candidates.Count; i++)
                    {
                        candidateNames[i] = candidateLabels[i].Text;
                    }

                    if (controller.dataController.addVote(new VoteData(candidateNames, preferences)))
                    {
                        return true;
                    }
                    else
                    {
                        // Couldn't add the candidate data. The preference choices are either out of the given preference range 
                        // or there is a duplicate preference choice. Prompt the user and cancel.
                    }
                }
            return false;
        }
        #endregion

        #endregion


        #region - Error Checks.

        #region - Check Inputs,
        /// <summary>
        /// Check the inputs to make sure they are all valid.
        /// </summary>
        /// <param name="preferences"></param>
        /// <returns></returns>
        private bool checkInputs(out int[] preferences)
        {
            bool canCreate = true;
            preferences = new int[candidateLabels.Length];
            
                for (int i = 0; i < candidatePreferenceTextBoxes.Length; i++)
                {
                    TextBox textBox = candidatePreferenceTextBoxes[i];

                    int voteValue = 0;
                    if (int.TryParse(textBox.Text, out voteValue))
                    {
                        preferences[i] = voteValue;
                    }
                }

                if (canCreate)
                {
                    return true;
                }
                return false;
        }
        #endregion


        #region - Error Check.
        /// <summary>
        /// Check that the user inputs don't have any errors
        /// </summary>
        /// <returns></returns>
        public bool errorCheck()
        {
            int voteValue = 0;
            bool emptyTextbox = false;
            returnBool = true;
            for (int i = 0; i < candidatePreferenceTextBoxes.Length; i++)
            {
                TextBox textBox = candidatePreferenceTextBoxes[i];

                if (String.IsNullOrEmpty(candidatePreferenceTextBoxes[i].Text))
                {
                    emptyTextbox = true;
                    error += (candidateLabels[i].Text + " preference is empty" + "\r\n");
                    returnBool = false;
                }
                else if (!int.TryParse(textBox.Text, out voteValue))
                {
                    error += (candidateLabels[i].Text + " preference value is not an integer" + "\r\n");
                    returnBool = false;
                }
                else
                {
                    if (Convert.ToString(candidatePreferenceTextBoxes[i]).Contains("0"))
                    {
                        Console.WriteLine("equal 0");
                        error += (candidateLabels[i].Text + " preference is less than 0, must be between 1 and " + candidatePreferenceTextBoxes.Length + "\r\n");
                        returnBool = false;
                    }
                    else if (int.Parse(textBox.Text) > candidatePreferenceTextBoxes.Length)
                    {
                        error += (candidateLabels[i].Text + " preference is greater than allocated values, must be between 1 and " + candidatePreferenceTextBoxes.Length + "\r\n");
                        returnBool = false;
                    }
                } 
            }
            if (emptyTextbox == false)
            {
                if (CheckForDuplicates() == true)
                {
                    error += "Two of same vote found, cannot input same name twice or more" + "\r\n";
                    returnBool = false;
                }
            }

            if (returnBool == false)
            {
                MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                error = "";
                return returnBool;
            }
            else
            {
                error = "";
                return returnBool;
            }
        }

        /// <summary>
        /// Check for duplicate inputs
        /// </summary>
        /// <returns></returns>
        public bool CheckForDuplicates()
        {
            //Collect all your TextBox objects in a new list...
            List<TextBox> textBoxes = new List<TextBox>();

            for (int i = 0; i < candidatePreferenceTextBoxes.Length; i++)
            {
                textBoxes.Add(candidatePreferenceTextBoxes[i]);
            }

            //Use LINQ to count duplicates in the list...
            int dupes = textBoxes.GroupBy(x => x.Text)
                                 .Where(g => g.Count() > 1)
                                 .Count();

            //true if duplicates found, otherwise false
            if (dupes > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #endregion


        #region - Clear Data.
        /// <summary>
        /// Clear the inputted data
        /// </summary>
        public void clearData()
        {

            for (int i = 0; i < candidatePreferenceTextBoxes.Length; i++)
            {
                if (!String.IsNullOrEmpty(candidatePreferenceTextBoxes[i].Text))
                {
                    candidatePreferenceTextBoxes[i].Text = "";
                }
                else
                {
                    error = ("Preferences hasn't been designated");
                }
            }
            MessageBox.Show(error);
        }
        #endregion
    }
}
