﻿/*
 * File Name : Home_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan 
 * 
 * Last Edit Date : 2:55 am 19/06/2017
 */

using System;

namespace ElectoralVotingApp.Controllers
{
    public class Home_Controller
    {
        #region - Variables.
        private Controller controller;
        #endregion


        #region - Constructor.
        public Home_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion
    }
}
