﻿/*
 * File Name : SpreadSheet_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:56 am 19/06/2017
 */

using System;
using System.Windows.Forms;

namespace ElectoralVotingApp.Controllers
{
    public class SpreadSheet_Controller
    {
        #region - Variables.
        private Controller controller;
        #endregion


        #region - Constructors.
        public SpreadSheet_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion


        #region - Start Spreadsheet Setup
        /// <summary>
        /// Start the spreadsheet creation
        /// </summary>
        public void start()
        {
            controller.EVForm.selectedDataRow = null;

            controller.EVForm.SpreadSheet_DataGridView.ReadOnly = true;

            controller.EVForm.SpreadSheet_DataGridView.DataSource = controller.dataController.getDataTable();
            if ((controller.EVForm.SpreadSheet_DataGridView.Width - 40) / controller.EVForm.SpreadSheet_DataGridView.Columns.Count >= 50)
            {
                foreach (DataGridViewColumn column in controller.EVForm.SpreadSheet_DataGridView.Columns)
                {
                    column.Width = (controller.EVForm.SpreadSheet_DataGridView.Width - 40) / controller.EVForm.SpreadSheet_DataGridView.Columns.Count;
                }
            }
            else
            {
                foreach (DataGridViewColumn column in controller.EVForm.SpreadSheet_DataGridView.Columns)
                {
                    column.Width = 50;
                }
            }

            controller.EVForm.SpreadSheet_CandidateCount_Text.Text = controller.dataController.candidates.Count.ToString();

            controller.EVForm.SpreadSheet_TotalVotes_Text.Text = controller.dataController.voteData.Count.ToString();
            controller.EVForm.SpreadSheet_InvalidVotes_Text.Text = controller.dataController.invalidVotes.Count + " / " + controller.dataController.voteData.Count;
        }
        #endregion
    }
}
