﻿/*
 * File Name : Setup_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:56 am 19/06/2017
 */

using ElectoralVotingApp.DataStructures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace ElectoralVotingApp.Controllers
{
    public class Setup_Controller
    {
        #region - Variables
        private Controller controller;

        public bool editingCandidates = false;

        private Label[] candidateLabels;
        private TextBox[] candidateNameTextBoxes;

        public String error = "";
        public bool returnBool = true;
        #endregion


        #region - Constructor.
        public Setup_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion


        #region - Start.
        /// <summary>
        /// start the create candidate process
        /// </summary>
        public void start()
        {
            if (candidateLabels != null &&
                candidateLabels.Length > 0)
            {
                foreach(Label label in candidateLabels)
                {
                    label.Dispose();
                }
            }
            if (candidateNameTextBoxes != null &&
                candidateNameTextBoxes.Length > 0)
            {
                foreach(TextBox textBox in candidateNameTextBoxes)
                {
                    textBox.Dispose();
                }
            }

            controller.EVForm.Setup_CandidateCount_Textbox.Text = "";
        }
        #endregion


        #region - Create Candidates.
        /// <summary>
        /// create the candidate fields
        /// </summary>
        /// <param name="amount"></param>
        public void createCandidateFields(int amount)
        {
            if (candidateLabels != null)
            {
                for (int i = 0; i < candidateLabels.Length; i++)
                { 
                    candidateLabels[i].Dispose();
                    candidateNameTextBoxes[i].Dispose();
                }
            }

            candidateLabels = new Label[amount];
            candidateNameTextBoxes = new TextBox[amount];

            for (int i = 0; i < amount; i++)
            {
                candidateLabels[i] = new Label();
                candidateLabels[i].MinimumSize = new Size(110, 20);
                candidateLabels[i].Parent = controller.EVForm.Setup_CandidateInformation_Scrollbox;
                candidateLabels[i].Location = new Point(20, 30 + (25 * (i - 1)));
                candidateLabels[i].Text = "Candidate " + (i + 1) + " Name";

                candidateNameTextBoxes[i] = new TextBox();
                candidateNameTextBoxes[i].Parent = controller.EVForm.Setup_CandidateInformation_Scrollbox;
                candidateNameTextBoxes[i].Size = new Size(120, 26);
                candidateNameTextBoxes[i].Location = new Point(130, 27 + (25 * (i - 1)));
            }
        }

        /// <summary>
        /// Create the candidate textboxes
        /// </summary>
        public void createCandidates()
        {
            List<CandidateData> candidates = new List<CandidateData>();

            controller.dataController.resetCandidates();

            // Use the content given from the candidate name text boxes.
            for (int i = 0; i < candidateNameTextBoxes.Length; i++)
            {
                controller.dataController.addCandidate(new CandidateData(candidateNameTextBoxes[i].Text)); 
            }

            controller.dataController.candidatesSet = true;

            controller.spreadSheetController.start();
        }
        #endregion


        #region - Error Checks.
        /// <summary>
        /// check user inputs for errors
        /// </summary>
        /// <returns>returnBool</returns>
        public bool errorCheck()
        {
            returnBool = true;
            bool emptyTextBox = false;
            for (int i = 0; i < candidateNameTextBoxes.Length; i++)
            {
                if (String.IsNullOrEmpty(candidateNameTextBoxes[i].Text)){
                    emptyTextBox = true;
                    error += "Candidate Name " + (i+1) + " is empty" + "\r\n";
                    returnBool = false;
                } 
            }

            if (emptyTextBox == false)
            {
                if (CheckForDuplicates() == true)
                {
                    error += "Two of same name found, cannot input same name twice or more";
                    returnBool = false;
                }
            }

            if (returnBool == false)
            {
                MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                error = "";
                return returnBool;
            }
            else
            {
                error = "";
                return returnBool;
            }
        }

        /// <summary>
        /// check to see if there are any duplidate names
        /// </summary>
        /// <returns>true/false</returns>
        public bool CheckForDuplicates()
        {
            //Collect all your TextBox objects in a new list...
            List<TextBox> textBoxes = new List<TextBox>();

            for (int i = 0; i < candidateNameTextBoxes.Length; i++)
            {
                textBoxes.Add(candidateNameTextBoxes[i]);
            }

            //Use LINQ to count duplicates in the list...
            int dupes = textBoxes.GroupBy(x => x.Text)
                                 .Where(g => g.Count() > 1)
                                 .Count();

            //true if duplicates found, otherwise false
            if (dupes > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region - Clear Textboxes.
        /// <summary>
        /// Clear data in textboxes
        /// </summary>
        public void clearData()
        {
            if (candidateNameTextBoxes != null)
            {
                for (int i = 0; i < candidateNameTextBoxes.Length; i++)
                {
                    if (!String.IsNullOrEmpty(candidateNameTextBoxes[i].Text))
                    {
                        candidateNameTextBoxes[i].Text = "";
                    }
                }
            }
            else
            {
                MessageBox.Show("Nothing to clear");
            }
        }
        #endregion
    }
}
