﻿/*
 * File Name : Statistics_Controller.cs 
 * 
 * Author : Kimberley McEwan & Tristyn Mackay
 * 
 * Date Created : 1:00 pm 11/06/2017 
 * 
 * Last Edit Author : Kimberley McEwan
 * 
 * Last Edit Date : 2:57 am 19/06/2017
 */

using ElectoralVotingApp.DataStructures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ElectoralVotingApp.Controllers
{
    public class Statistics_Controller
    {
        #region - Variables.
        private Controller controller;
        #endregion


        #region - Constructors.
        public Statistics_Controller(Controller controller)
        {
            this.controller = controller;
        }
        #endregion


        #region - Create Statistics.
        /// <summary>
        /// Used to produce the statistics for rounds then display those statistics.
        /// </summary>
        public void produceStatistics()
        {
            // Clear the tabs.
            controller.EVForm.Statistics_TabPanel.TabPages.Clear();

            CandidateData[] candidates = new CandidateData[controller.dataController.candidates.Count];
            for (int i = 0; i < controller.dataController.candidates.Count; i++)
            {
                candidates[i] = controller.dataController.candidates[i];
            }
            VoteData[] votes = new VoteData[controller.dataController.validVotes.Count];
            for (int i = 0; i < controller.dataController.validVotes.Count; i++)
            {
                votes[i] = controller.dataController.validVotes[i];
            }

            VoteRoundData roundData = new VoteRoundData(candidates, votes);

            displayRound(roundData);
        }
        #endregion


        #region - Display Rounds.
        /// <summary>
        /// Used for displaying round data in a round tab.
        /// </summary>
        /// <param name="roundData"></param>
        private void displayRound(VoteRoundData roundData)
        {
            // Create the Tab.
            TabPage tabPage = new TabPage();
            tabPage.Name = "Statistics_Round" + roundData.roundNumber + "_Tab";
            tabPage.BackColor = Color.White;
            
            
            // Create the Tab title.
            Label tabTitle = new Label();
            tabTitle.Parent = tabPage;
            if (roundData.nextRound != null)
            {
                tabTitle.Text = "Round " + roundData.roundNumber;
                tabPage.Text = "Round " + roundData.roundNumber;
            }
            else
            {
                tabTitle.Text = "Result";
                tabPage.Text = "Result";
            }
            tabTitle.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            tabTitle.Location = new Point(218, 6);

            // Create the Information panel.
            Panel informationPanel = new Panel();
            informationPanel.Parent = tabPage;
            informationPanel.Location = new Point(4, 31);
            informationPanel.Size = new Size(489, 188);
            informationPanel.BackColor = Color.Transparent;

            // Create the ranking Label.
            Label rankingLabel = new Label();
            rankingLabel.Parent = informationPanel;
            rankingLabel.Text = "Rankings";
            rankingLabel.Font = new Font(Label.DefaultFont, FontStyle.Bold);
            rankingLabel.Location = new Point(21, 18);
            rankingLabel.BackColor = Color.Transparent;

            // Create the Votes title label.
            Label voteLabel = new Label();
            voteLabel.Parent = informationPanel;
            voteLabel.Text = "Votes";
            voteLabel.Font = new Font(Label.DefaultFont, FontStyle.Bold);
            voteLabel.Location = new Point(148, 18);
            voteLabel.Size = new Size(60, 13);
            voteLabel.BackColor = Color.Transparent;

            // Create the pie chart.
            Chart pieChart = new Chart();
            pieChart.Palette = ChartColorPalette.EarthTones;
            pieChart.BackColor = Color.LightBlue;
            pieChart.ChartAreas.Add(new ChartArea());
            pieChart.Titles.Add("Candidate Votes");
            pieChart.Titles[0].Font = new Font(Label.DefaultFont, FontStyle.Bold);
            pieChart.ChartAreas[0].BackColor = Color.Transparent;
            Series series1 = new Series
            {
                Name = "Series1",
                IsVisibleInLegend = true,
                Color = Color.Transparent,
                ChartType = SeriesChartType.Pie
            };
            pieChart.Series.Add(series1);
            pieChart.Series[0].Points.Clear();
            pieChart.Parent = informationPanel;
            pieChart.Location = new Point(210, 12);
            pieChart.Size = new Size(232, 156);
            pieChart.BackColor = Color.Transparent;

            // Create the candidates with their rankings.
            List<CandidateData> candidateRankings = new List<CandidateData>();
            List<int> candidateVoteCounts = new List<int>();
            for (int i = 0; i < roundData.candidates.Length; i++)
            {
                if (candidateRankings.Count == 0)
                {
                    candidateRankings.Add(roundData.candidates[i]);
                    candidateVoteCounts.Add(roundData.candidateVoteCount[i]);
                }
                else {
                    for (int k = 0; k < candidateVoteCounts.Count; k++)
                    {
                        if (k != candidateVoteCounts.Count - 1)
                        {
                            if (roundData.candidateVoteCount[i] > candidateVoteCounts[k])
                            {
                                candidateRankings.Insert(k, roundData.candidates[i]);
                                candidateVoteCounts.Insert(k, roundData.candidateVoteCount[i]);
                                break;
                            }
                        }
                        else if (k == candidateVoteCounts.Count - 1)
                        {
                            if (roundData.candidateVoteCount[i] >= candidateVoteCounts[k])
                            {
                                candidateRankings.Insert(k, roundData.candidates[i]);
                                candidateVoteCounts.Insert(k, roundData.candidateVoteCount[i]);
                                break;
                            }
                            else
                            {
                                candidateRankings.Add(roundData.candidates[i]);
                                candidateVoteCounts.Add(roundData.candidateVoteCount[i]);
                                break;
                            }
                        }
                    }
                }
            }


            // Create the candidates and Votes labels ordered by most to least votes,
            // then add them to the pie chart.
            Point rankingLabelStartLocation = new Point(21, 40);
            Point voteLabelStartLocation = new Point(148, 40);
            int labelHeightDiff = 25;
            Random rand = new Random();

            for (int i = 0; i < candidateRankings.Count; i++)
            {
                Label candidateLabel = new Label();
                candidateLabel.Parent = informationPanel;
                candidateLabel.Text = (i + 1) + ": " + candidateRankings[i].candidateName;
                candidateLabel.Location = new Point(rankingLabelStartLocation.X, rankingLabelStartLocation.Y + (i * labelHeightDiff));
                candidateLabel.BackColor = Color.Transparent;

                Label voteAmountLabel = new Label();
                voteAmountLabel.Parent = informationPanel;
                voteAmountLabel.Text = candidateVoteCounts[i].ToString();
                voteAmountLabel.Location = new Point(voteLabelStartLocation.X, voteLabelStartLocation.Y + (i * labelHeightDiff));
                voteAmountLabel.BackColor = Color.Transparent;

                // Set up the pie chart components.
                pieChart.Series[0].Points.Add(candidateVoteCounts[i]);
                pieChart.Series[0].Points[i].LegendText = candidateRankings[i].candidateName;
                pieChart.Legends.Add(pieChart.Series[0].Points[i].LegendText);
                if (candidateVoteCounts[i] > 0)
                {
                    pieChart.Series[0].Points[i].AxisLabel = candidateVoteCounts[i].ToString();
                }
            }

            pieChart.Invalidate();

            controller.EVForm.Statistics_TabPanel.TabPages.Add(tabPage);
            
            if (roundData.nextRound != null)
            {
                displayRound(roundData.nextRound);
            }
        }
        #endregion
    }
}