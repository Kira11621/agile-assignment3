﻿using System;
using ElectoralVotingApp.Controllers;

namespace ElectoralVotingApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            new Controller();
        }
    }
}
